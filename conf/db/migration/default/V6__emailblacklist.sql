CREATE TABLE IF NOT EXISTS `email_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

