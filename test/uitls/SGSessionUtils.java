/**
 *
 */
package uitls;

import java.util.UUID;

import models.SmartGenie;

/**
 * @author carloxwang
 *
 */
public class SGSessionUtils {
	
	public static String init(SmartGenie sg){
		
		String sessionId = UUID.randomUUID().toString();
		
		return controllers.api.v1.sg.cache.SGSessionUtils.login(sessionId, sg.id)?sessionId:"";
	}
}
