package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;
import static play.test.Helpers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

public class SgControllerTest extends BaseModelTest{
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void initial_mac(){

		String macAddress = uu.smartGenie1.macAddress;
		String firmwareGroupName = uu.sgFirmwareGroup.name;
    	String url = "/api/sg/init";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sgMac", macAddress);
    	data.put("firmwareGroupName", firmwareGroupName);
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void initial_sgId(){
		String firmwareGroupName = uu.sgFirmwareGroup.name;
    	String url = "/api/sg/init";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("smartGenieId", uu.smartGenie1.id);
    	data.put("firmwareGroupName", firmwareGroupName);
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void addDevice(){
		String sessionId = uu.sgSession1;
		
    	String url = "/api/sg/dev";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("xGenieId", uu.xGenie1.id);
    	data.put("firmwareGroupName", uu.sgFirmwareGroup.name);
    	data.put("xGenieType", "1");
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void addDevice_fail(){
		String sessionId = uu.sgSession1;
		
    	String url = "/api/sg/dev";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("xGenieId", uu.xGenie1.id);
    	data.put("firmwareGroupName", "sasasamila");
    	data.put("xGenieType", "-1001");
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
    	
    	assertThat(contentAsString(result).contains("-14001")).isEqualTo(true);
	}
	
	@Test
	public void removeDevice(){

		String sessionId = uu.sgSession1;
		
    	String url = "/api/sg/dev";

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sessionId : " + sessionId);

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "url : " + url);
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("xGenieId", "{\"xGenie\":[{\"xgId\":\"" + uu.xGenie1.macAddr + "\"}]}");
    	
    	Result result = route(fakeRequest(Helpers.DELETE, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void keepALive(){

		String sessionId = uu.sgSession1;
		
    	String url = "/api/sg/route";
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sessionId : " + sessionId);

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "url : " + url);
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								.withHeader("content-Type", "application/json")
    								.withFormUrlEncodedBody(data));
    	
    	assertThat(status(result) == 200).isEqualTo(true);
	}
	
	@Test
	public void resetSg(){

		String sessionId = uu.sgSession1;
		
    	String url = "/api/sg/reset";
		LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sessionId : " + sessionId);
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "url : " + url);
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	
    	Result result = route(fakeRequest(Helpers.DELETE, url)
    								.withHeader("content-Type", "application/json")
    								.withFormUrlEncodedBody(data));
    	
    	assertThat(status(result) == 200).isEqualTo(true);
	}
	
	
	
	@Test
	public void getSgLocation(){

		String sessionId = uu.sgSession1;
		LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sessionId : " + sessionId);
    	
    	String url = "/api/sg/location?" +
				"sessionId=" + sessionId
				;

    	Result result = route(fakeRequest(Helpers.GET, url)
							 .withHeader("content-Type", "application/json"));
			
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
}
