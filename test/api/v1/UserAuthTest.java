/**
 * 
 */
package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.util.HashMap;
import java.util.Map;

import models.PnRelationship;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

/**
 * @author johnwu
 * @version 創建時間：2013/8/26 上午9:52:07
 */
public class UserAuthTest extends BaseModelTest {
	private UserUtils uu;
	
	@Before
	public void init(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void loginTestSuccess(){
		String url = "/api/app/login";
		
		Map<String, String> data = new HashMap<String, String>();
    	data.put("loginId", uu.ui1.identData);
    	data.put("loginPwd", "123698745");
    	data.put("loginType", "email");
    	data.put("pnId", "123698745");
    	data.put("deviceOsName", "android");
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
				 .withHeader("content-Type", "application/json")
				 .withFormUrlEncodedBody(data));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void loginTestFailure(){
		String url = "/api/app/login";
		
		Map<String, String> data = new HashMap<String, String>();
    	data.put("loginId", uu.ui1.identData + "1");
    	data.put("loginPwd", uu.ui1.password);
    	data.put("loginType", uu.ui1.identType.name());
    	data.put("pnId", "123698745");
    	data.put("deviceOsName", "android");
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
				 .withHeader("content-Type", "application/json")
				 .withFormUrlEncodedBody(data));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));

    	assertThat(contentAsString(result).contains("-10002")).isEqualTo(true);
	}
	
	@Test
	public void logoutTestSuccess(){
		String url = "/api/app/logout";

		Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", uu.session1);
    	
    	Result result = route(fakeRequest(Helpers.DELETE, url)
				 .withHeader("content-Type", "application/json")
				 .withFormUrlEncodedBody(data));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "logoutTestSuccess : " + contentAsString(result));

    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
}
