package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;
import static play.test.Helpers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

public class UserControllerTest extends BaseModelTest{
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void setMyInfo(){
		
    	String url = "/api/app/user";
    	
    	String sessionId = uu.session1;
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("nickname", "BaMeiMei9BooBaTou");
    	data.put("loginPwd", "password");
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void getMyInfo(){
		
		String sessionId = uu.session1;
		
    	String url = "/api/app/user?" +
    					"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json"));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void chkAccountAvailableExisted(){
		
    	String url = "/api/app/account/check?" +
    					"loginId=" + "holajohn9@gmail.com";
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json"));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("-10001")).isEqualTo(true);
	}
	
	@Test
	public void chkAccountAvailableNone(){
		
    	String url = "/api/app/account/check?" +
    					"loginId=" + "holajohn11@gmail.com";
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json"));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void getNetworkInfo(){
		
		String sessionId = uu.session1;
		
    	String url = "/api/app/sg/network/info?" +
    					"sessionId=" + sessionId + "&" +
    					"innIp=" + "192.168.1.10" + "&" +
    					"netMask=" + "255.255.255.0" + "&" +
    					"wifiMacAddress=" + "7829304acb45" + "&" +
    					"sgMac=" + uu.smartGenie3.macAddress;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json"));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	/*
	@Test
	public void setSgLocation(){

		String sessionId = uu.session1;
		
    	String url = "/api/app/sg/location";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("macAddr", uu.smartGenie1.macAddress);
    	data.put("loclat", "25.87251");
    	data.put("loclng", "121.02189");
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								.withHeader("content-Type", "application/json")
    								.withFormUrlEncodedBody(data));
    	
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	*/
}
