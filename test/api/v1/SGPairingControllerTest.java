package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;

import uitls.UserUtils;
import utils.LoggingUtils;


public class SGPairingControllerTest extends BaseModelTest {
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void doPairing(){
		String sessionId = uu.session1;
    	String macAddr = uu.unpairSG.macAddress;
    	
    	String url = "/api/app/user/dev_pairing";

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sessionId : " + sessionId);
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "macAddr : " + macAddr);
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "url : " + url);

    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("macAddr", macAddr);
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void doPairing_fail(){
		String sessionId = uu.session1;
    	String macAddr = uu.smartGenie2.macAddress;
    	
    	String url = "/api/app/user/dev_pairing";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("macAddr", macAddr);
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "doPairing_fail() : " + contentAsString(result));
    	assertThat(contentAsString(result).contains("-12004")).isEqualTo(true);
	}
	
	@Test
	public void unPairing(){
		String sessionId = uu.session1;
    	String macAddr = uu.smartGenie1.macAddress;
    	
    	String url = "/api/app/user/dev_unpairing";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("macAddr", macAddr);
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void unPairing_fail(){
		String sessionId = uu.session1;
    	String macAddr = uu.smartGenie2.macAddress;
    	
    	String url = "/api/app/user/dev_unpairing";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("macAddr", macAddr);
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "unPairing_fail() : " + contentAsString(result));
    	assertThat(contentAsString(result).contains("-12002")).isEqualTo(true);
	}
}
