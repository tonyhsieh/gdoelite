/**
 *
 */
package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;

import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;

/**
 * @author carloxwang
 *
 */


public class SGConfigurationAPITest extends BaseModelTest {

	private UserUtils uu;

	private final static String CFG = "<Yo><Whatsup></Whatsup><KrKrKr>科科科</KrKrKr></Yo>";

	@Before
	public void setupData(){
		uu = new UserUtils();
		uu.initial();
	}
/*
	@Test
	public void testBackup() throws JsonParseException, JsonMappingException, IOException{
		String url = "/api/sg/cfg";

		Map<String, String> map = new HashMap<String, String>();
		map.put("sessionId", uu.sgSession1);
		map.put("cfg", CFG);


		Result result = route(fakeRequest(Helpers.PUT, url)
								.withFormUrlEncodedBody(map));

		assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);

    	// test restore
    	restore(uu.smartGenie1.macAddress);
	}


	public void restore(String sgMac) throws JsonParseException, JsonMappingException, IOException{
		String url = "/api/sg/cfg?" +
						"sgMac=" + sgMac;

		Result result = route(fakeRequest(Helpers.GET, url));

		String response = contentAsString(result);

		assertThat(StringUtils.isNotBlank(response) && response.contains("SUCCESS")).isEqualTo(true);

		// Test download URL
//		ObjectMapper om = new ObjectMapper();
//
//		SGCfgRespPack pack = om.readValue(response, SGCfgRespPack.class);
//
//		Result downloadResult = route(fakeRequest(Helpers.GET, pack.downloadUrl));
//
//		assertThat(status(downloadResult) == 200);

	}
*/
}
