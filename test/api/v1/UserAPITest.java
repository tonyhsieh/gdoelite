/**
 *
 */
package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import models.UserIdentifier;

import org.junit.Test;

import com.avaje.ebean.Ebean;

/**
 * @author carloxwang
 *
 */
public class UserAPITest extends BaseModelTest {

	@Test
	public void testReg(){
		UserIdentifier ui = Ebean.find(UserIdentifier.class, "123");

		assertThat(ui == null).isEqualTo(true);
	}
}
