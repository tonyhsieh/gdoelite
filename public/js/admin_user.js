
;(function(window, $) {

	window.adminUser = (function(){

		var ops = {};

		var options = function(_ops) {
			var defaultOps = {
				deleteUserUrl: ''
			}

			if(_ops){
				$.extend(ops, defaultOps, _ops);
			}

			return this;
		};

		var deleteUser = function(obj){
			var $obj = $(obj);
			var name = $obj.attr('data-name');
			var email = $obj.attr('data-email');
			
			bootbox.confirm("Are You Sure Delete All Of Device?<br>Name："　+ name +"<br>Email："　+ email, 
			function(result){
				if(result){
					var url = ops.deleteUserUrl.replace('userid', $obj.attr('data-id'));
					location.href = url;
				}
			});
			
			return false;
		}
		
		var deleteWhiteUser = function(obj){
			var email_selected = $('#cmb_email').find(":selected").text();
			
			bootbox.confirm("Are You Sure Delete User?<br>Email："　+ email_selected, 
			function(result){
				if(result){
					$('#reg-form').submit();
				}
			});
			
			return false;
		}
		
		return {
			options: options,
			deleteUser: deleteUser,
			deleteWhiteUser: deleteWhiteUser
		}

	})();

}(window, jQuery));