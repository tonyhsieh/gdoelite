import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.sg.cache.SGSessionUtils;
import frameworks.Constants;
import models.SmartGenie;
import models.User;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import utils.LogUtil;
import utils.LoggingUtils;
import utils.MyUtil;
import utils.OMManager;

import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import java.lang.reflect.Method;

import javax.inject.Inject;

public class ActionCreator implements play.http.ActionCreator
{
    @Inject
    private FormFactory formFactory;

    @Override
    public Action createAction(Http.Request request, Method actionMethod)
    {
        return new Action.Simple()
        {
            @Override
            public CompletionStage<Result> call(Http.Context ctx)
            {
                CharSequence cs1 = "admin";
                CharSequence cs2 = "pwd";
                CharSequence cs3 = "assets";
                if(!ctx.request().uri().contains(cs1) && !ctx.request().uri().contains(cs2) && !ctx.request().uri().contains(cs3)) {
                    try {
                        DynamicForm dynamicForm = formFactory.form().bindFromRequest(ctx.request());
                        Map<String, String> data = dynamicForm.data();
                        String sessionId = "";
                        for (Map.Entry<String, String> entry : data.entrySet()) {
                            if (entry.getKey().equalsIgnoreCase("sessionId")) {
                                sessionId = entry.getValue();
                            }
                        }

                        ObjectMapper mapper = OMManager.getInstance().getObjectMapper();

                        ObjectNode node = mapper.getNodeFactory().objectNode();
                        if (!node.has("extIp")) {
                            node.put("extIp", MyUtil.getClientIp(request));
                        }
                        if (InetAddress.getLocalHost() != null) {
                            node.put("host", InetAddress.getLocalHost().getHostName());
                        }
                        node.put("method", request.method());
                        node.put("uri", request.uri());
                        node.put("reqBody", mapper.writeValueAsString(data));

                        if (sessionId.length() > 0) {
                            if (Http.Context.current().args.get(Constants.API_V1_APP_SESSION_HTTP_ARGS_KEY) != null) {
                                User user = UserSessionUtils.getUser(sessionId);
                                if (user != null) {
                                    node.put("type", "APPToCloud");
                                    node.put("userEmail", user.email);
                                    node.put("userName", user.nickname);
                                }
                            }

                            if (Http.Context.current().args.get(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY) != null) {
                                SmartGenie sg = SGSessionUtils.getSmartGenie(sessionId);
                                if (sg != null) {
                                    node.put("type", "HEToCloud");
                                    node.put("heMac", sg.macAddress);
                                    node.put("heName", sg.name);
                                }
                            }
                        }

                        LogUtil.log(node.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return delegate.call(ctx);
            }
        };
    }
}
