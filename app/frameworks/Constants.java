/**
 *
 */
package frameworks;

/**
 * @author carloxwang
 *
 */
public abstract class Constants {

	public final static String JSON_CALENDAR_PATTERN = "yyyy/MM/dd HH:mm:ss.SSSZ";

	public final static String API_V1_APP_REQ_PACK_HTTP_ARGS_KEY = "api.v1.app.reqPack.http.args.key";
	public final static String API_V1_APP_REQ_LOG_HTTP_ARGS_KEY = "api.v1.app.reqLogPack.http.args.key";

	public final static String API_V1_APP_SESSION_HTTP_ARGS_KEY = "api.v1.app.session.http.args.key";
	public final static String API_V1_SG_SESSION_HTTP_ARGS_KEY = "api.v1.sg.session.http.args.key";
	public final static String WEB_ADMIN_SESSION_HTTP_ARGS_KEY = "web.admin.session.http.args.key";

	public final static String API_V1_SG_SESSION_SG_ID_KEY = "api.v1.sg.session.sg.id.key";


	public final static int PASSWORD_LENGTH = 6;

	//public final static int PING_TIMER = 30 * 1000;	//30 sec
	public final static int PING_TIMER = 150 * 1000;	//90 sec
	
	public final static String EVENT_LOG_DATA_ADD_DEVICE = "Add Device";
	public final static String EVENT_LOG_DATA_REMOVE_DEVICE = "Remove Device";



	public final static String SESSION_KEY_USER = "SESSION_KEY_USER";
}
