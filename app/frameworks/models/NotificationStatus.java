package frameworks.models;

public enum NotificationStatus {
	BUILD(100),
	PROCESS(101),
	PRESEND(102),
	SEND(103),
	RESPONSE(104)
	;
	
	public final int value;
	NotificationStatus(int value) {
        this.value = value;
    }
}
