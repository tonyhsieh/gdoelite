package frameworks.models;

public class DataFileType {
	
	public static String AG_CFG = "AG_CFG";
	
	public static String IRRIGATION_CONTROLLER = "IRRIGATION_CONTROLLER";
	
	public static boolean hasDataFileType(String dataFileTypeIn){
		
		String[] InUseDataFileType = {
				"AG_CFG",
				"IRRIGATION_CONTROLLER"
			};
		
		String dataFileType = dataFileTypeIn.toUpperCase();
		
		for(int i = 0; i < InUseDataFileType.length; i++){
			if(InUseDataFileType[i].equals(dataFileType)){
				return true;
			}
		}
		return false;
	}
}
