package frameworks.models;

/**
 * @author carloxwang
 */
public enum StatusCode
{

    /**
     * StatusCode Table:
     * <p>
     * [General Information]
     * Negative: -1 ~ -99
     * Positive: 1 ~ 99
     * <p>
     * [API v1]
     * Negative: -10000 ~ -99999
     * Positive: 10000 ~ 99999
     */

    GENERAL_SUCCESS(1),
    UNKNOWN_ERROR(-1),
    BAD_REQUEST(-400),
    FORBIDDEN(-403),
    NOT_FOUND(-404),
    INTERNAL_SERVER_ERROR(-500),

    //////// API Used /////////
    USER_DUPLICATED(-10001),                        //使用者登入資訊重複
    USER_NOTFOUND(-10002),                          //使用者找無
    USER_PASSWORD_ERROR(-10003),                    //使用者密碼錯誤
    AUTH_LOGOUT_ERROR(-10004),                      //登出錯誤

    SESSION_ERROR(-10005),                          //session 找無
    SESSION_FAILURE(-10006),                        //session 過期,錯誤
    PASSWORD_REQUARE(-10007),                       //密碼為必填欄位
    PASSWORD_LENGTH_REQUARE(-10008),                //密碼為必填欄位
    PASSWORD_NOTREQUARE(-10009),                    //登入方式不需要密碼
    EMAIL_FORMAT_ERROR(-10010),                     //Email格式不符合
    USER_RELATIONSHIP_ERROR(-10011),
    FORM_DATA_ERROR(-10012),

    FORMAT_ERROR(-10013),                            //和定義的格式不符合
    ENCODE_ERROR(-10014),                            //編碼不正確

    RESET_PWD_SESSION_EXPIRED(-11001),              //重設密碼之認證碼已失效
    WRONG_PARAMETER(-11002),                        //參數不正確
    EMAIL2TEXT_PIN_EXPIRED(-11003),                 // Email2Text PIN碼已失效
    EMAIL2TEXT_WRONG_PIN(-11004),                   // Email2Text PIN碼錯誤
    PHONE_NUMBER_VALIDATION_FAILED(-11005),         // 電話號碼驗證失敗
    PHONE_NUMBER_VALIDATION_UNAVAILABLE(-11006),    // 電話號碼驗證服務不可用
    USER_WITH_NO_PHONE_NUMBER(-11007),
    //////// API Used - smart genie for app /////////
    SMARTGENIE_NOTFOUND(-12001),                    //smart genie 找無
    SMARTGENIE_OWNERSHIP_ERROR(-12002),             //非擁有者
    SMARTGENIE_IS_JOINED(-12003),                   //已加入
    SMARTGENIE_IS_PAIRING(-12004),                  //smart genie 已經配對
    SMARTGENIE_OWNERSHIP_EMPTY(-12005),             //smart genie尚未配對
    SMARTGENIE_NOTFOUND_FWGROUPNAME(-12006),
    SMARTGENIE_CFG_NOTFOUND(-12007),                //找不到configuration的備份記錄
    SMARTGENIE_MEMBERSHIP_ERROR(-12008),            //傳來比對的 userId 非此 SmartGenie 的 Owner 或 User
    DATA_TYPE_NOTFOUND(-12009),                        //找不到 data type
    FILE_NOTFOUND(-12010),                            //找不到指定的 file
    DEVICE_NOTFOUND(-12201),                        //All AG, XG ...(any device), not found
    UNKNOWN_STATUS(-12202),                            //unknown status
    FILE_DELETE_ERROR(-12203),
    ////////API Used - smart genie for xGenie /////////
    XGENIE_DUPLICATED(-14001),                      //此XGenie已經被加入到別的SmartGenie了
    XGENIE_REMOVED(-14002),                         //此XGenie已經被移除
    XGENIE_NOTFOUND(-14003),                        //xgenie 找無
    SMARTGENIE_XGENIE_NOTMARCH(-14004),             //SmartGenie並非未加入此XGenie
    CONFIGURATION_NOTFOUND(-14005),                    //Configuration 找不到
    CONFIGURATION_TYPE_NOTFOUND(-14006),            //Configuration Type 找不到
    CONFIGURATION_XGENIE_NOTMATCH(-14008),
    PARENT_XGENIE_NOTFOUND(-14007),                    //Parent XGenie 找無
    CONFIGURATION_CANNOTSAVE(-14009),
    ////////API Used - contact list /////////
    CONTACT_TYPE_NOTFOUND(-16001),                  //找不到 contact list type
    CONTACT_RELATIONSHIP_NOTFOUNF(-16002),          //找不到 contact list relationship
    CONTACT_DATA_DUPLICATED(-16003),                //已有 contact data
    CONTACT_GROUP_ADD_FAIL(-16004),                 //無法新增Contact Group
    CONTACT_GROUP_DATA_DUPLICATE(-16005),           //新增的Contact Group內容有重覆
    CONTACT_GROUP_NOTFOUND(-16006),                 //找不到Contact Group
    CONTACT_GROUP_ID_NOT_MARCH(-16007),             //Contact Group ID 錯誤
    REGISTER_EMAIL_NOTFOUND(-16008),                //找不到註冊的Email
    PUSH_NOTIFICATION_NOT_WORK(-16009),             //無法使用 Push Notification
    CONTACT_GROUP_OWNER_DUPLICATE(-16010),          //Contact Group Owner 重覆
    CONTACT_MEMBER_NOTFOUND(-16011),                //無法找到連絡人
    SENDHUB_SERVICE_FAILURE(-16012),
    COUNTRY_CODE_NOT_FOUND(-16013),                    // 找不到country code
    COUNTRY_CODE_NOT_AVAILABLE(-16014),                // country code not available

    ///////Thirty Party ERROR/////////////
    AWS_S3_OPERATION_ERROR(-17001),                    //AWS S3 操作錯誤

    //////Database ERROR//////////////////
    CAN_NOT_WRITE_TO_DATABASE(-18001),                //無法寫入資料庫

    SMARTGENIEID_CREATE_ERROR(-92005)               //SHA-1建立SmartGenie ID錯誤
    ;

    public int value;

    StatusCode(int value)
    {
        this.value = value;
    }
}