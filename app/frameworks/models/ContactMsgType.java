package frameworks.models;

public enum ContactMsgType {

	CRITICAL,
	WARNING,
	INFO
	;
}