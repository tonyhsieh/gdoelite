package frameworks.models;

public enum SgRemoveXgType {

	SUCCESS(1),
	FAILURE(-1)
	;
	
	public final int value;
	SgRemoveXgType(int value) {
        this.value = value;
    }
}
