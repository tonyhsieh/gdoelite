package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

import play.data.validation.Constraints;

@Entity
@Table(name = "ic_event_stats")
public class IcEventStats extends BaseEntity {

	@Id
	@GeneratedValue
	private int id;
	
	@Constraints.Required
	@Column(name = "data_id", columnDefinition = "TINYINT")
	private byte dataId; // -1: Hourly, 0: Daily, 1: Weekly, 2: Monthly, 3: Yearly
	
	@Constraints.Required
	@Column(name = "xgenie_id", length = 64)
	private String xGenieId;
	
	@Constraints.Required
	@Column(name = "zone_id", columnDefinition = "TINYINT")
	private byte zoneId; // 1: Zone 1, 2: Zone 2, 3: Zone 3, 4: Zone 4, 5: Zone 5, 6: Zone 6
	
	@Constraints.Required
	@Column(name = "event_type", columnDefinition = "SMALLINT")
	private int eventType;
	
	@Constraints.Required
	@Column(nullable = false)
	private Date date;
	
	@Column(name = "cum_mins")
	private int cumulativeMinutes; // Unit: minutes; Minimum: 1
	
	public IcEventStats(byte dataId, String xGenieId, byte zoneId, int eventType, Date date, int cumulativeMinutes) {
		this.dataId = dataId;
		this.xGenieId = xGenieId;
		this.zoneId = zoneId;
		this.eventType = eventType;
		this.date = date;
		this.cumulativeMinutes = cumulativeMinutes;
	}
	
	public byte getDataId() {
		return dataId;
	}

	public String getxGenieId() {
		return xGenieId;
	}

	public byte getZoneId() {
		return zoneId;
	}

	public int getEventType() {
		return eventType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setCumulativeMinutes(int cumulativeMinutes) {
		this.cumulativeMinutes = cumulativeMinutes;
	}
	
	public int getCumulativeMinutes() {
		return cumulativeMinutes;
	}

	// For weekly or monthly stats
	public final static List<IcEventStats> find(byte dataId, String xGenieId, byte zoneId, int eventType, Date startDate, Date endDate) {
		
		List<IcEventStats> lstStats = Ebean.find(IcEventStats.class)
				.where()
				.eq("data_id", dataId)
				.eq("xgenie_id", xGenieId)
				.eq("zone_id", zoneId)
				.eq("event_type", eventType)
				.between("date", startDate, endDate)
				.findList();

				return lstStats;
	}
	
	public final static IcEventStats find(byte dataId, String xGenieId, byte zoneId, int eventType, Date date) {
		
		IcEventStats stats = Ebean.find(IcEventStats.class)
				.where()
				.eq("data_id", dataId)
				.eq("xgenie_id", xGenieId)
				.eq("zone_id", zoneId)
				.eq("event_type", eventType)
				.eq("date", date)
				.findUnique();

				return stats;
	}
	
	public final static List<String> findAllXGenieId() {
		SqlQuery q = Ebean.createSqlQuery("select distinct xgenie_id from ic_event_stats");
		List<SqlRow> rows = q.findList();
		int size = rows.size();
		List<String> lstXId = new ArrayList<>(size);
		for (SqlRow row : rows ) {
			lstXId.add(row.getString("xgenie_id"));
		}
		
		return lstXId;
	}
	
}
