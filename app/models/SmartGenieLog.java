package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;

@Entity
public class SmartGenieLog extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 5841673848873183103L;

	@Id
	@Constraints.Required
	public String id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="smart_genie_id", nullable=false, insertable=false, updatable=false)
	public SmartGenie smartGenie;

	public Calendar recordDateTime;

	public Calendar logDateTime;

	public String extIp;

	public String innIp;

	public String netMask;

	public String wifiMacAddress;
	
	public String wifiIP;
	
	public String ssidName;
	
	public String firmwareVer;
	
	public int p2pSuccess;
	
	public int p2pFail;
	
	public String lat;
	
	public String lng;
	
	public Integer timezone;

	public final static int updateTimestamp(String sgId, String time){
		String ql = "UPDATE smart_genie_log SET update_at = :time, log_date_time = :time, record_date_time = :time WHERE id = :sgId ;" ;

		return Ebean.createSqlUpdate(ql).setParameter("sgId", sgId).setParameter("time", time).execute();
	}


	public final static SmartGenieLog find(String sgId){
		return Ebean.find(SmartGenieLog.class, sgId);
	}
	
	public final static SmartGenieLog findLocation(String sgId){
		
		Query<SmartGenieLog> query = Ebean.createQuery(SmartGenieLog.class);
		
		query.setId(sgId);
		
		query.select("lat,lng");
		
		return query.findUnique();
	}

	public final static List<SmartGenieLog> findAll(){
		return Ebean.createQuery(SmartGenieLog.class)
				.findList();
	}

	public static List<String> findSgIdList(){
		List<String> sgIdList = new ArrayList<String>();
		for (SmartGenieLog sgLog : findAll()){
			sgIdList.add(sgLog.id);
		}
		return sgIdList;
	}




}
