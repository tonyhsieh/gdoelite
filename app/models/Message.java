package models;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.persistence.*;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import javax.validation.constraints.NotNull;

import play.data.validation.*;

@Entity
public class Message extends BaseEntity {

	/**
	 * @author john
	 */
	private static final long serialVersionUID = 8504181707852558866L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;

	@ManyToOne
	@JoinColumn(name="receiver_Id", nullable=false)
	public User receiver;

	public Integer senderType;

	public String senderId;

	public String content;

	public Boolean deliverStatus;

	@Column(name="send_at", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
	public Calendar sendAt;

	public static List<Message> listByUser(String receiverId){
		return Ebean.find(Message.class).where(
			Expr.eq("receiver_Id", receiverId)
			).findList();
	}

	public static List<Message> listByUser(String receiverId, Boolean deliverStatus){
		return Ebean.find(Message.class)
					.where()
					.eq("receiver_Id", receiverId)
					.eq("deliverStatus", deliverStatus)
					.findList();

	}
}
