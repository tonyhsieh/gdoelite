package models;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Query;
import play.data.validation.Constraints;

@Entity
@Table(name="contact")
public class Contact extends BaseEntity{

    /**
     * 
     */
    private static final long serialVersionUID = -2749987973447625274L;

    @Id
    @Constraints.Required
    @Constraints.Max(40)
    public String id;

    @ManyToOne
    @JoinColumn(name="event_contact_id", nullable=false)
    @Constraints.Required
    public EventContact eventContact;

    public boolean notifyEmail;
    
    public boolean notifySMS;
    
    public boolean notifyPushNotification;
    
    public String userId;

    @Override
    public String toString() {
        return "Contact{" +
                "id='" + id + '\'' +
                ", eventContact=" + eventContact +
                ", notifyEmail=" + notifyEmail +
                ", notifySMS=" + notifySMS +
                ", notifyPushNotification=" + notifyPushNotification +
                ", userId='" + userId + '\'' +
                '}';
    }

    public final static List<Contact> listByUserId(String userId){
        return Ebean.createQuery(Contact.class)
            .where(Expr.eq("user_id", userId))
            .findList();
    }
    
    public static List<Contact> listByEventContact(String ecId){
    	return Ebean.find(Contact.class)
    			.where()
    			.eq("event_contact_id", ecId)
    			.findList();
    }
    
    public static List<Contact> listByEventContactNUserId(String ecId, String userId){
    	return Ebean.find(Contact.class)
    			.where()
    			.eq("event_contact_id", ecId)
    			.eq("user_id", userId)
    			.findList();
    }
    
    public static Contact findByEventContactNUserId(String ecId, String userId){
    	return Ebean.find(Contact.class)
    			.where()
    			.eq("event_contact_id", ecId)
    			.eq("user_id", userId)
    			.findUnique();
    }
}