/**
 * 
 */
package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.PagedList;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * @author johnwu
 * @version 創建時間：2013/9/12 下午1:10:02
 */

@Entity
@Table(name="pn_relationship")
public class PnRelationship extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8563095972387149641L;

	@Id
	@Constraints.Required
    public String pn;
	
	@ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    @Constraints.Required
	public User user;
	
	@Constraints.Required
	public String pnId;
	
	@Constraints.Required
	public String deviceOsName;
	
	public String devId;
	
	public String appCode;

	@Override
	public String toString() {
		return "PnRelationship{" +
				"pn='" + pn + '\'' +
				", user=" + user +
				", pnId='" + pnId + '\'' +
				", deviceOsName='" + deviceOsName + '\'' +
				", devId='" + devId + '\'' +
				", appCode='" + appCode + '\'' +
				'}';
	}

	//註記一
	public static List<PnRelationship> listByUser(User user){
		return Ebean.find(PnRelationship.class)
				.where()
				.eq("user", user)
				.findList();
	}
	
	public static List<PnRelationship> listByPn(String pnId){
		return Ebean.find(PnRelationship.class)
				.where()
				.eq("pnId", pnId)
				.findList();
	}
	
	public final static List<PnRelationship> listByUserAndOs(User user, String os){
		return Ebean.find(PnRelationship.class)
				.where()
				.eq("user", user)
				.eq("deviceOsName", os)
				.findList();
	}
	
	public static List<PnRelationship> listByDevId(String devId){
		return Ebean.find(PnRelationship.class)
				.where()
				.eq("dev_id", devId)
				.findList();
	}
	
	public static List<PnRelationship> listByOsName(String osName){
		return Ebean.find(PnRelationship.class)
				.where()
				.eq("deviceOsName", osName)
				.findList();
	}

	//check page function
	//
	public static PagedList<PnRelationship> pnPage(String orderBy, Boolean asc, Integer page, Integer pageSize){
		com.avaje.ebean.Query<PnRelationship> query = Ebean.find(PnRelationship.class);
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<PnRelationship> pagingList = query.findPagedList(page,pageSize);

		return pagingList;
	}

	public static PagedList<PnRelationship> pnPageWithPnId(String userId, String orderBy, Boolean asc, Integer page, Integer pageSize){
		com.avaje.ebean.Query<PnRelationship> query = Ebean.find(PnRelationship.class)
				.where(Expr.eq("user_id", userId));

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<PnRelationship> pagingList = query.findPagedList(page,pageSize);

		return pagingList;
	}
}
