package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import play.data.validation.Constraints;

@Entity
@Table(name="push_notification_list")
public class PushNotificationList extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4589950105321872283L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public String id;

	public String pnId;		//Registe ID
	
	public String userSessionId;
	
	public String contactId;
	
	
	public final static List<PushNotificationList> findByContactId(String contactId){
		return Ebean.createQuery(PushNotificationList.class)
				.where(Expr.eq("contactId", contactId))
				.findList();
	}
	
	public final static List<PushNotificationList> findByUserSessionId(String userSessionId){
		return Ebean.createQuery(PushNotificationList.class)
				.where(Expr.eq("userSessionId", userSessionId))
				.findList();
	}
	
	public final static List<PushNotificationList> listByContactNPn(String contactId, String pn){
		return Ebean.find(PushNotificationList.class)
				.where()
				.eq("contact_id", contactId)
				.eq("pn_id", pn)
				.findList();
	}

	public final static PushNotificationList findByContactNPn(String contactId, String pn){
		return Ebean.find(PushNotificationList.class)
				.where()
				.eq("contact_id", contactId)
				.eq("pn_id", pn)
				.findUnique();
	}
}
