package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import com.avaje.ebean.*;
import com.avaje.ebean.Query;

import org.springframework.beans.support.PagedListHolder;
import play.data.validation.Constraints;

@Entity
public class Employee extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -41168794976451966L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public String id;

	public String name;
	
	public String department;
	
	public String cellPhone;
	
	public String phoneExtension;
	
	public String email;
	
	
	public final static Employee find(String empId){
		return Ebean.find(Employee.class, empId);
	}
	
	public static PagedList<Employee> emPage(String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<Employee> query = Ebean.find(Employee.class);

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<Employee> pagingList = query.findPagedList(page, pageSize);



		return pagingList;
	}
	
	public static PagedList<Employee> emPageWithEmail(String email, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<Employee> query = Ebean.find(Employee.class)
								.where(Expr.eq("email", email));

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<Employee> pagingList = query.findPagedList(page, pageSize);


		return pagingList;
	}
	
}
