package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import play.data.validation.Constraints;

@Entity
public class XGenieRelationship extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4783948518644300454L;
	
	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;
	
	@Constraints.Required
	public String smartGenieId;
	
	@Constraints.Required
	public String parentMacAddr;
	
	@Constraints.Required
	public String childMacAddr;
	
	public String parentParam;
	
	public final static List<XGenieRelationship> listXgByParentXgId(String parentXgId){
		return Ebean.find(XGenieRelationship.class)
					.where(
						Expr.eq("parentMacAddr", parentXgId)
						).findList();
	}
	
	public final static List<XGenieRelationship> listXgByChileXgId(String childXgId){
		return Ebean.find(XGenieRelationship.class)
					.where(
						Expr.eq("childMacAddr", childXgId)
						).findList();
	}
}
