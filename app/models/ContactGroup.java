package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

@Entity
@Table(name="contact_group")
public class ContactGroup extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8220698076391739591L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;

	@Constraints.Required
	public String name;

	@Constraints.Required
	public String sgId;

	@Constraints.Required
	public String msgType;
	
	public final static ContactGroup findByContactGroupId(String contactGroupId){
		return Ebean.createQuery(ContactGroup.class)
				.where(Expr.eq("id", contactGroupId))
				.findUnique();
	}
	
	public final static List<ContactGroup> findBySgId(String sgId){
		return Ebean.createQuery(ContactGroup.class)
				.where(Expr.eq("sg_id", sgId))
				.findList();
	}
	
	public final static boolean isDuplicated(String sgId, String name, String msgType){
		List<ContactGroup> lstContactGroup = Ebean.find(ContactGroup.class)
				//.where(Expr.eq("id", sgId))
				.where()
				.eq("sg_id", sgId)
				.findList();

		//有新增成功回傳 false		
		//無新增成功回傳 true
		ContactGroup contactGroup = null;
		for(int i = 0; i < lstContactGroup.size(); i++){
			contactGroup = lstContactGroup.get(i);
			
			if((contactGroup.name.equals(name) && contactGroup.msgType.equalsIgnoreCase(msgType)) && contactGroup.sgId.equals(sgId)){
				return true;
			}
		}
		return false;
	}
	
	public static List<ContactGroup> listContactGroupBySgNType(String sgId, String msgType){
		return Ebean.find(ContactGroup.class)
					.where()
					.eq("sg_id", sgId)
					.eq("msg_type", msgType)
					.findList();	
	}
	
	public static ContactGroup findBySGNType(String sgId, String msgType){
		return Ebean.find(ContactGroup.class)
				.where()
				.eq("sg_id", sgId)
				.eq("msg_type", msgType)
				.findUnique();
	}
}

