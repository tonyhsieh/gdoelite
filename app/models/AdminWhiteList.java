/**
 *
 */
package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;

/**
 * @author carloxwang
 *
 */
@Entity
@Table(name="admin_white_list")
public class AdminWhiteList extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 6652608281527246367L;
	@Id
	public String email;

	public static final AdminWhiteList find(String email){
		return Ebean.find(AdminWhiteList.class, email);
	}
	
	public static final List<AdminWhiteList> list(){
		return Ebean.find(AdminWhiteList.class).findList();
	}
}
