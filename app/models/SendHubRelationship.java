/**
 * 
 */
package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

import play.data.validation.Constraints;

/**
 * @author johnwu
 * @version 創建時間：2013/8/16 下午5:09:10
 */

@Entity
public class SendHubRelationship extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9163996170243910736L;

	@Id
	@Constraints.Required
	public String cellphone;
	
	@Constraints.Required
	public String shId;
	
	public static SendHubRelationship findByCellPhone(String cellphone){
		return Ebean.find(SendHubRelationship.class).where()
				.eq("cellphone", cellphone)
				.findUnique();
	}
}
