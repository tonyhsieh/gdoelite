package models;

import java.util.List;
import javax.persistence.*;
import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;

@Entity
public class SmartGenie extends BaseEntity {

	/**
	 * @author john
	 */
	private static final long serialVersionUID = 1926027915958820013L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public String id;

	public String name;

	@Constraints.Required
	public String macAddress;

	public boolean isPairing;

	@ManyToOne
	@JoinColumn(name="owner_id", nullable=true)
	public User owner;

	@ManyToOne
	@JoinColumn(name="firmware_group_id", nullable=false)
	public FirmwareGroup firmwareGroup;

	public String info;
	
	public String activeCode;
	
	public Integer devType;


    @OneToOne
    @JoinColumn(name="photo_file_id")
    public FileEntity photoFileId;

	@Column(name="auto_upgrade")
	public Boolean autoUpgrade;

    @Column(name="auto_upgrade_time")
    public String autoUpgradeTime;

    @Column(name="auto_upgrade_once_time")
    public String autoUpgradeOnceTime;

    @Column(name="auto_upgrade_Once")
    public Boolean autoUpgradeOnce;
	

	public final static SmartGenie findByMacAddress(String macAddr){
		return Ebean.find(SmartGenie.class).where()
						.eq("mac_address", macAddr)
						.findUnique();
	}
	
	public final static List<SmartGenie> listByFG(FirmwareGroup fg){
		return Ebean.find(SmartGenie.class)
				.where()
				.eq("firmwareGroup", fg)
				.findList();
	}

	public final static SmartGenie find(String sgId){
		return Ebean.find(SmartGenie.class, sgId);
	}

	public final static List<SmartGenie> listSmartGenieByOwner(String ownerId){
		return Ebean.createQuery(SmartGenie.class)
				.where(Expr.eq("owner_id", ownerId))
				.findList();
	}

	public final static boolean checkOwnership(String userId, String smartGenieId){
		SmartGenie result = Ebean.createQuery(SmartGenie.class)
				.where(
					Expr.and(
						Expr.eq("owner_id", userId),
						Expr.eq("id", smartGenieId)
						)).findUnique();

		return (result != null);
	}

	public final static boolean hasXGenie(String smartGenieId, String xGenieId){
		return (Ebean.find(XGenie.class)
					.where(
						Expr.and(
							Expr.eq("smartGenie_id", smartGenieId),
							Expr.eq("id", xGenieId))
					)
					.findUnique()
					!= null);
	}
	
	public static List<SmartGenie> listAll(){
		return Ebean.find(SmartGenie.class).findList();
	}

	/*
	@Override
	public String toString() {
		return "SmartGenie [id=" + id + ", name=" + name + ", macAddress="
				+ macAddress + ", isPairing=" + isPairing + ", owner=" + owner
				+ ", firmwareGroup=" + firmwareGroup + "]";
	}
	*/
	
	public static PagedList<SmartGenie> sgPage(String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<SmartGenie> query = Ebean.find(SmartGenie.class);

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<SmartGenie> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}
	
	public static PagedList<SmartGenie> sgPageWithMacAddr(String macAddr, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<SmartGenie> query = Ebean.find(SmartGenie.class)
					.where(Expr.eq("macAddress", macAddr));
		
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<SmartGenie> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}	
	
	public static PagedList<SmartGenie> sgPageWithUser(String userId, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<SmartGenie> query = Ebean.find(SmartGenie.class)
					.where(Expr.eq("owner_id", userId));
		
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<SmartGenie> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}
	
	public static PagedList<SmartGenie> sgPageWithSGenieId(String smartGenieId, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<SmartGenie> query = Ebean.find(SmartGenie.class)
					.where(Expr.eq("id", smartGenieId));
		
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<SmartGenie> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}
	
}

