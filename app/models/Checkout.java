package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

import play.data.validation.Constraints;

@Entity
public class Checkout extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2814083874238888051L;

	@Id
	@Constraints.Max(40)
	public String id;
	
	//@Constraints.Required
	public String no;
	
	public String price;
	
	public String tax;
	
	public String total;
	
	public String pay;
	
	public String table_no;
	
	public final static List<Checkout> findAll(){
		return Ebean.createQuery(Checkout.class)
				.findList();
	}
	
	/*
	 create table checkout (
id                        varchar(40) not null,
no		          varchar(40),
price		          varchar(40),
tax		          varchar(40),
total		          varchar(40),
pay		          varchar(40),
table_no		          varchar(40),
create_at                 datetime not null,
update_at                 datetime,
update_user_id            varchar(64),
status                    integer not null,
create_user_id            varchar(64) not null,
opt_lock                  bigint not null,
constraint checkout primary key (id));
	 */
}
