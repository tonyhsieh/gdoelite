package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.codec.digest.DigestUtils;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

@Entity
public class UserRelationship extends BaseEntity
{
    private static final long serialVersionUID = 1348752600338887174L;

    @Id
    @Constraints.Required
    public String id;

    @Constraints.Required
    public String ownerId;

    @Constraints.Required
    public String userId;

    public String smartGenieId;

    @Constraints.Required
    public String nickname;

    @Constraints.Required
    public String countryCode;

    @Constraints.Required
    public String cellPhone;

    @Constraints.Required
    public String email;

    @ManyToOne
    @JoinColumn(name = "gateway_id")
    public Gateway gateway;

    public static String genUserRelationshipId(String smartGenieId, String ownerId, String userId)
    {
        return DigestUtils.sha1Hex(smartGenieId + ownerId + userId + "what'up up up");
    }

    public static List<UserRelationship> listUserRelationshipByUserOrOwner(String userId)
    {
        return Ebean.find(UserRelationship.class)
                .where()
                .or(Expr.eq("userId", userId), Expr.eq("ownerId", userId))
                .findList();
    }

    public static List<UserRelationship> listUserRelationshipByUser(String userId)
    {
        return Ebean.find(UserRelationship.class)
                .where(
                        Expr.eq("userId", userId)
                ).findList();
    }

    public static List<UserRelationship> listUserRelationshipByOwner(String ownerId)
    {
        return Ebean.find(UserRelationship.class)
                .where(
                        Expr.eq("ownerId", ownerId)
                ).findList();
    }

    public static List<UserRelationship> listUserRelationshipBySgNOwner(String sgId, String ownerId)
    {
        return Ebean.find(UserRelationship.class)
                .where(
                        Expr.and(
                                Expr.eq("smartGenieId", sgId),
                                Expr.eq("ownerId", ownerId)
                        )).findList();
    }

    public static List<UserRelationship> listUserRelationshipBySG(String smartGenieId)
    {
        return Ebean.find(UserRelationship.class)
                .where(
                        Expr.eq("smart_genie_id", smartGenieId)
                ).findList();
    }

    public static List<UserRelationship> listUserRelationshipByOwnerNUser(String ownerId, String userId)
    {
        return Ebean.find(UserRelationship.class)
                .where(
                        Expr.and(
                                Expr.eq("ownerId", ownerId),
                                Expr.eq("userId", userId)
                        )).findList();
    }

    public static boolean isJoined(String smartGenieId, String userId)
    {
        return (Ebean.find(UserRelationship.class)
                .where(
                        Expr.and(
                                Expr.eq("user_id", userId),
                                Expr.eq("smart_genie_id", smartGenieId))
                ).findUnique()
                != null);
    }

    public static UserRelationship findByUserNSmartGenie(String ownerId, String userId, String smartGenieId)
    {
        Map<String, Object> constrains = new HashMap<>();
        constrains.put("owner_id", ownerId);
        constrains.put("user_id", userId);
        constrains.put("smart_genie_id", smartGenieId);

        return Ebean.find(UserRelationship.class)
                .where(
                        Expr.allEq(constrains)
                ).findUnique();
    }

    public static UserRelationship findByMemberIdNSmartGenie(String memberId, SmartGenie smartGenie)
    {
        Map<String, Object> constrains = new HashMap<String, Object>();

        if (smartGenie.owner == null)
        {
            return null;
        }

        List<UserRelationship> listUserRelationship = null;

        try
        {
            if (smartGenie.owner.id.toString().equals(memberId))
            {
                constrains.put("owner_id", memberId);
                constrains.put("smart_genie_id", smartGenie.id);

                listUserRelationship = Ebean.find(UserRelationship.class)
                        .where(
                                Expr.allEq(constrains)
                        ).findList();

                return listUserRelationship.get(0);
            }
        }
        catch (Exception err)
        {
            err.printStackTrace();
        }

        UserRelationship userRelationship = null;
        if (listUserRelationship == null)
        {
            constrains = new HashMap<>();

            constrains.put("user_id", memberId);
            constrains.put("smart_genie_id", smartGenie.id);
            userRelationship = Ebean.find(UserRelationship.class)
                    .where(
                            Expr.allEq(constrains)
                    ).findUnique();
        }
        return userRelationship;
    }
}
