package models;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang3.time.DateFormatUtils;



import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.*;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;

import frameworks.Constants;

@Entity
public class SmartGenieActionHistory extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9217413406518962036L;

	@Id
	public UUID id;
	
	public String smartGenieId;
	
	public String action;
	
	public String form;
	
	public static PagedList<SmartGenieActionHistory> sgHistoryPage(String sgId, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -7);
		
		Query<SmartGenieActionHistory> query = Ebean.find(SmartGenieActionHistory.class)
				.where(Expr.and(Expr.gt("createAt", DateFormatUtils.format(calendar, Constants.JSON_CALENDAR_PATTERN)), 
						Expr.eq("smartGenieId", sgId)));

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}
		
		PagedList<SmartGenieActionHistory> pagingList = query.findPagedList(page,pageSize);
		
		return pagingList;
	}
	
	public final static List<SmartGenieActionHistory> findByBeforeDate(Date date){
		return Ebean.find(SmartGenieActionHistory.class)
    			.where()
    			.lt("createAt", date)
    			.findList();
	}
}
