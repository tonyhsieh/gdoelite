package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

@Entity
public class DevicePositionLog extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5758068097215364315L;

	@Id
	@Constraints.Required
	public UUID id;
	
	@Constraints.Required
	public UUID userDevLogId;

	public String mcc;

	public String mnc;

	public String lac;

	public String cellId;

	public String lng;
	
	public String lat;
	
	public static DevicePositionLog getDevicePositionLogByUserDevLogIdAndCurrectTime(UUID userDevLogId){
		
		List<DevicePositionLog> listDevicePositionLog = Ebean.createQuery(DevicePositionLog.class)
				.where(Expr.eq("userDevLogId", userDevLogId))
				.order().desc("create_at")
				.findList();
		
		DevicePositionLog devicePositionLog = listDevicePositionLog.get(0);
		return devicePositionLog;
	}
}
