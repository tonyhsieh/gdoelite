package models;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

@Entity
public class AppActionHistory extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1122657007656202462L;

	@Id
	public UUID id;
	
	public String userId;
	
	public String action;
	
	public String form;
	
	public final static List<AppActionHistory> findByBeforeDate(Date date){
		return Ebean.find(AppActionHistory.class)
    			.where()
    			.lt("createAt", date)
    			.findList();
	}
}
