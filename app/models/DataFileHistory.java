package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;

@Entity
public class DataFileHistory extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8028363517095189575L;

	@Id
	@Constraints.Max(40)
	public UUID id;
	
	@Constraints.Required
	public String data_file_id;
	
	@Constraints.Required
	public String content_type;
	
	@Constraints.Required
	public String sg_id;
	
	@Constraints.Required
	public String file_id;

	public String descript;
	
	@Constraints.Required
	public String data_type;
	
	@Constraints.Required
	public String data_name;
}
