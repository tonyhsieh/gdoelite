package services;

import java.io.IOException;


import utils.db.FileEntityManager;
import frameworks.models.DataFileType;
import frameworks.models.FileType;

public class ConfigurationService {

	public static String DOWNLOAD_BY_URL = "URL";
	public static String DOWNLOAD_BY_JSON = "JSON";
	
	public static void backupFile(String data, String fileName, String fileType) throws IOException{
		
		if(fileType.toUpperCase().equals(DataFileType.IRRIGATION_CONTROLLER)){
			FileEntityManager feo = new FileEntityManager(fileName, data, FileType.CONFIGURATION_IRRIGATION_CONTROLLER, fileType);
		}else if(fileType.toUpperCase().equals(DataFileType.AG_CFG)){
			FileEntityManager feo = new FileEntityManager(fileName, data, FileType.SG_CONFIGURATION, fileType);
		}
	}

	public static String getFile(String fileId, String fileType) throws IOException{
		return FileEntityManager.getStringFromTextFile(fileId, FileType.CONFIGURATION_IRRIGATION_CONTROLLER, fileType);
	}
	
	public static String getFile(String fileName, String fileType, String downloadType) throws IOException{
		
		
		String strDownloadType = ConfigurationService.DOWNLOAD_BY_URL;
		
		if(downloadType!=null){
			if(downloadType.toUpperCase().equals(ConfigurationService.DOWNLOAD_BY_URL)){
				strDownloadType = ConfigurationService.DOWNLOAD_BY_URL;
			}else if(downloadType.toUpperCase().equals(ConfigurationService.DOWNLOAD_BY_JSON)){
				strDownloadType = ConfigurationService.DOWNLOAD_BY_JSON;
			}
		}
		
		if(strDownloadType.equals(ConfigurationService.DOWNLOAD_BY_URL)){

			if(fileType.toUpperCase().equals(DataFileType.IRRIGATION_CONTROLLER)){
				return FileEntityManager.getDownloadUrl(fileName, FileType.CONFIGURATION_IRRIGATION_CONTROLLER, fileType);
			}else if(fileType.toUpperCase().equals(DataFileType.AG_CFG)){
				return FileEntityManager.getDownloadUrl(fileName, FileType.SG_CONFIGURATION, fileType);
			}
			
		}else if(strDownloadType.equals(ConfigurationService.DOWNLOAD_BY_JSON)){
			
			if(fileType.toUpperCase().equals(DataFileType.IRRIGATION_CONTROLLER)){
				return FileEntityManager.getStringFromTextFile(fileName, FileType.CONFIGURATION_IRRIGATION_CONTROLLER, fileType);
			}else if(fileType.toUpperCase().equals(DataFileType.AG_CFG)){
				return FileEntityManager.getStringFromTextFile(fileName, FileType.SG_CONFIGURATION, fileType);
			}
			
		}
		
		return null;
	}
	
	/*
	public static void backupCfg(String jsonData, String cfgName, String cfgType) throws IOException{
		
		FileEntityManager feo = new FileEntityManager(cfgName, jsonData, FileType.CONFIGURATION_IRRIGATION_CONTROLLER, cfgType);
	}

	public static String getCfg(String cfgId, String cfgType) throws IOException{
		
		return FileEntityManager.getStringFromTextFile(cfgId, FileType.CONFIGURATION_IRRIGATION_CONTROLLER, cfgType);
	}
	*/
}
