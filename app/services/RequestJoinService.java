package services;

import java.util.List;

import models.RequestJoin;
import frameworks.models.RequestJoinType;

/**
 *
 * @author johnwu
 *
 */
public abstract class RequestJoinService{


	public static List<RequestJoin> listRequestJoinByUserNType(String userId, RequestJoinType type){
		return RequestJoin.listRequestJoinByUserNType(userId, type);
	}
}
