package services;

import java.util.List;

import models.SmartGenie;
import models.UserRelationship;
import models.XGenie;

import com.avaje.ebean.Ebean;
import org.apache.commons.lang3.exception.ExceptionUtils;
import utils.LoggingUtils;

/**
 *
 * @author johnwu
 *
 */

public abstract class SmartGenieService{

	public static UserRelationship addNewUser(String ownerId, String targetUserId){
		UserRelationship urs = new UserRelationship();

		urs.ownerId = ownerId;
		urs.userId = targetUserId;

		Ebean.save(urs);

		return urs;
	}

	public static SmartGenie findByMacAddress(String macAddr){
		return SmartGenie.findByMacAddress(macAddr);

	}

	public static List<SmartGenie> listSmartGenieByOwner(String ownerId){
		return SmartGenie.listSmartGenieByOwner(ownerId);
	}

	public static boolean checkOwnership(String userId, String smartGenieId){
		return SmartGenie.checkOwnership(userId, smartGenieId);
	}

	public static boolean addNewDevice(String macAddress, SmartGenie smartGenie, String firmwareGroupName, String XGenieType){
		XGenie xGenie = XGenie.isDuplicated(macAddress, smartGenie, firmwareGroupName, XGenieType);

		if(xGenie==null){
			return false;
		}
		else{
			//寫入dB
			xGenie.save();
			return true;
		}
	}

    public static boolean addNewDevicev2(String macAddress, SmartGenie smartGenie, String firmwareGroupName, String XGenieType, String XgenieName){
        XGenie xGenie = XGenie.isDuplicatedv2(macAddress, smartGenie, firmwareGroupName, XGenieType, XgenieName);

        if(xGenie == null){
            return false;
        } else{
            try{
                xGenie.save();
                return true;
            }catch (Exception err){
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "add new device error " + ExceptionUtils.getStackTrace(err));
                return  false;
            }
        }
    }




	public static boolean hasXGenie(String smartGenieId, String xGenieId){
		return SmartGenie.hasXGenie(smartGenieId, xGenieId);
	}
}
