import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import controllers.socket.P2PServer;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import com.amazonaws.services.sqs.model.Message;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;

import controllers.api.v1.app.Email2TextController;
import controllers.api.v1.app.IcEventStatsDaily;
import controllers.api.v1.app.IcEventStatsMonthly;
import controllers.api.v1.sg.DeviceEventLoggerController;
import frameworks.Constants;
import frameworks.models.CacheKey;
import frameworks.models.ContactType;
import frameworks.models.EventLogType;
import frameworks.models.IcZoneId;

import com.amazonaws.services.sqs.model.InvalidIdFormatException;
import com.amazonaws.services.sqs.model.OverLimitException;
import com.amazonaws.services.sqs.model.ReceiptHandleIsInvalidException;

import models.Contact;
import models.DuplicateEventLog;
import models.Event;
import models.EventContact;
import models.IcEventStats;
import models.SmartGenie;
import models.SmartGenieLog;
import models.User;
import models.UserRelationship;
import models.XGenie;

import play.Configuration;
import play.libs.ws.*;

import akka.actor.ActorSystem;

import scala.concurrent.duration.Duration;
import utils.*;
import utils.LoggingUtils;
import utils.generator.CredentialGenerator;
import utils.notification.DeviceEventQueueMsg;
import utils.notification.EmailNotificationFactory;
import utils.notification.GCMNotificationFactory;
import utils.notification.NotificationFactory;
import utils.notification.SMSNotificationFactory;

/**
 * @author carloxwang
 */
@Singleton
public class Global {
    private final WSClient ws;
    private final ActorSystem actorSystem;
    private final Configuration configuration;
    private Thread p2pThread;
    @Inject
    public Global(WSClient ws, ActorSystem actorSystem, Configuration configuration) {
        this.ws = ws;
        this.actorSystem = actorSystem;
        this.configuration = configuration;
        onStart();
    }

    private static Map<ContactType, NotificationFactory> notificationMap = new HashMap<>();

    static {
        try {
            //Local Development sometimes Internet terrible. Add the try catch for avoid error.
            notificationMap.put(ContactType.EMAIL, new EmailNotificationFactory());
            notificationMap.put(ContactType.PUSH_NOTIFICATION, new GCMNotificationFactory());
            notificationMap.put(ContactType.SMS, new SMSNotificationFactory());
        } catch (Exception err) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, ExceptionUtil.exceptionStacktraceToString(err));
            err.printStackTrace();
        }
    }

    private void onStart() {
        int icSchedulerHour = configuration.getInt("ic.scheduler.hour");
        int icSchedulerMinute = configuration.getInt("ic.scheduler.minute");
        // String queueName = ConfigUtil.getQueueName1();

        if (configuration.getBoolean("application.notification")) {
            // consumeMsgQueue(queueName);
        }

        MonitorSmartGenie();
        MonitorEc2RdsStorage();
        NettyStart();

        // Scheduler for inserting daily IcEventStats
        actorSystem.scheduler().schedule(
                // Daily cron job
                Duration.create(nextExecutionInSeconds(icSchedulerHour, icSchedulerMinute), TimeUnit.SECONDS),
                Duration.create(24, TimeUnit.HOURS), () ->
                {
                    Calendar c = Calendar.getInstance();
                    Calendar cm = Calendar.getInstance();
                    boolean beginOfMonth = false;
                    if (c.get(Calendar.DATE) == 1) {
                        beginOfMonth = true;
                    }

                    c.set(Calendar.DATE, c.get(Calendar.DATE) - 1);
                    IcEventStatsDaily icEventStatsDaily = new IcEventStatsDaily(c);

                    cm.set(Calendar.MONTH, cm.get(Calendar.MONTH) - 1);
                    IcEventStatsMonthly icEventStatsMonthly = new IcEventStatsMonthly(cm);

                    List<String> lstXId = IcEventStats.findAllXGenieId();
                    for (String xGenieId : lstXId) {
                        for (byte zoneId = IcZoneId.toValue(IcZoneId.ZONE_1);
                             zoneId <= IcZoneId.toValue(IcZoneId.ZONE_6); zoneId++) {
                            for (int eventType = EventLogType.PASSIVE_DEVICE_IRRI_WATERING.value;
                                 eventType <= EventLogType.PASSIVE_DEVICE_IRRI_WATERSAVING.value; eventType++) {
                                icEventStatsDaily.insertDailyStats(xGenieId, zoneId, eventType);
                                if (beginOfMonth) {
                                    icEventStatsMonthly.insertMonthlyStats(xGenieId, zoneId, eventType);
                                }
                            }
                        }
                    }
                }
                ,
                actorSystem.dispatcher()
        );
    }

    private static int nextExecutionInSeconds(int hour, int minute) {
        return Seconds.secondsBetween(
                new DateTime(),
                nextExecution(hour, minute)
        ).getSeconds();
    }

    private static DateTime nextExecution(int hour, int minute) {
        DateTime next = new DateTime()
                .withHourOfDay(hour)
                .withMinuteOfHour(minute)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);

        return (next.isBeforeNow())
                ? next.plusHours(24)
                : next;
    }

    public void NettyStart() {
        int port_number = configuration.getInt("p2p.socket.port");
        p2pThread = new Thread(new P2PServer(port_number, configuration));
        p2pThread.start();
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "p2pThread.start()");

        // Scheduler for deleting expired session keys
        /*
        actorSystem.scheduler().schedule(Duration.create(nextExecutionInSeconds(3, 0), TimeUnit.SECONDS), Duration.create(24, TimeUnit.HOURS), () ->
        {
            // Cron job executes at 3 am everyday
            String logHeader = "[Global] Akka.system().scheduler() - ";
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "Scheduler for deleting expired session keys");
            SessionKeyManager sessionKeyManager = SessionKeyManager.getInstance();
            sessionKeyManager.deleteExpiredKeys();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "Executed sessionKeyManager.deleteExpiredKeys()");
        }, actorSystem.dispatcher());

        // Scheduler for revoking expired session keys
        Akka.system().scheduler().schedule(
                Duration.create(30, TimeUnit.MINUTES),     //Initial delay 30 minutes
                Duration.create(30, TimeUnit.MINUTES),     //Frequency 30 minutes
                new Runnable() {
                    @Override
                    public void run() {
                        String logHeader = "[Global] Akka.system().scheduler() - ";
                        Logger.info(logHeader + "Scheduler for revoking expired session keys");
                        SessionKeyManager sessionKeyManager = SessionKeyManager.getInstance();
                        sessionKeyManager.revokeExpiredKeys();
                        Logger.info(logHeader + "Executed sessionKeyManager.deleteExpiredKeys()");
                    }
                },
                Akka.system().dispatcher()
        );*/
    }



    public void consumeMsgQueue(final String queueName) {
        String queueRegion = ConfigUtil.getQueueRegion();
        final SQSManager sqsManager = SQSManager.getInstance(queueRegion);
        final String queueUrl = sqsManager.getQueue(queueName);

        actorSystem.scheduler().schedule(Duration.create(new Random().nextInt(5), TimeUnit.SECONDS), Duration.create(2, TimeUnit.SECONDS), () ->
                {
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "queueUrl : " + queueUrl);
                    String environment = ConfigUtil.getApplicationEnvironment();
                    if (environment.equals("PROD")) {
                        environment = "";
                    }
                    Calendar start_time = Calendar.getInstance();
                    SimpleDateFormat tfs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                            "consumeMsgQueue starts at" + " " + tfs.format(start_time.getTime()));

                    boolean isMsgidExist;
                    List<Message> messages = new ArrayList<>();
                    if (queueUrl != null) {
                        String strMsgId;
                        try {
                            messages = sqsManager.receiveMessage(queueUrl);
                        } catch (OverLimitException err) {
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                    "sqs receive message OverLimit "
                                            + ExceptionUtil.exceptionStacktraceToString(err));
                        } catch (Exception err) {
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                    "sqs receive message "
                                            + ExceptionUtil.exceptionStacktraceToString(err));
                        }
                        if (messages.size() > 0) {
                            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "Message Size in " + queueName + " : " + String.valueOf(messages.size()));
                        }
                        for (Message message : messages) {
                            try {
                                isMsgidExist = false;
                                String msgBody = message.getBody();
                                LoggingUtils.log(LoggingUtils.LogLevel.INFO,
                                        "message body : " + msgBody);
                                ObjectMapper om = new ObjectMapper();
                                DeviceEventQueueMsg msg = om.readValue(msgBody, DeviceEventQueueMsg.class);
                                strMsgId = msg.id;
                                LoggingUtils.log(LoggingUtils.LogLevel.INFO,
                                        "strMsgId " + strMsgId);

                                if (CacheManager.get(CacheKey.MSG_QUEUE_ID, msg.id) != null) {
                                    LoggingUtils.log(LoggingUtils.LogLevel.INFO,
                                            "Msg queue duplicate msgId:" + msg.sgId);
                                    SmartGenie sg = SmartGenie.find(msg.sgId);
                                    DuplicateEventLog duplicateEventLog = new DuplicateEventLog();
                                    duplicateEventLog.eventType = msg.eventType;
                                    duplicateEventLog.smartGenie = sg;
                                    duplicateEventLog.msgId = msg.id;
                                    duplicateEventLog.save();

                                    sqsManager.deleteMessage(queueUrl, message);

                                    isMsgidExist = true;
                                } else {
                                    LoggingUtils.log(LoggingUtils.LogLevel.INFO,
                                            "Msg queue send msgId:" + msg.sgId);
                                    CacheManager.setWithId(CacheKey.MSG_QUEUE_ID, msg.id, msg.id,
                                            CacheManager.EVENT_ID_EXPIRED_TIME);
                                }

                                if (msg.listOwnerEmail == null) {
                                    //message sent by AG
                                    if (!isMsgidExist) {
                                        Event event = Event.findByType(msg.eventType);

                                        if (StringUtils.isNotBlank(msg.xGenieMac)) {
                                            //has xGenieMac
                                            XGenie xGenie = XGenie.findByXgMac(msg.xGenieMac);
                                            if (xGenie != null && xGenie.smartGenie != null) {
                                                SmartGenie smartGenie = Ebean.find(SmartGenie.class,
                                                        xGenie.smartGenie.id);

                                                EventContact ec = Ebean.find(EventContact.class,
                                                        CredentialGenerator.genEventContactId(
                                                                xGenie.macAddr, msg.eventType));

                                                if (ec != null) {
                                                    List<Contact> contactList = Contact.listByEventContact(ec.id);

                                                    for (Contact contact : contactList) {
                                                        UserRelationship ur = UserRelationship.findByUserNSmartGenie(
                                                                smartGenie.owner.id.toString(),
                                                                contact.userId,
                                                                smartGenie.id);

                                                        if (contact.notifyEmail) {
                                                            if (!"".equals(strMsgId)) {
                                                                notificationMap.get(ContactType.EMAIL).pushNotification(
                                                                        (ur != null) ? ur.email : smartGenie.owner.email,
                                                                        "Passive Device Notification",
                                                                        environment + xGenie.name + " " + event.eventMsg +
                                                                                " " + ",Message ID:" + strMsgId);
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment
                                                                        + " AG send " + " email to "
                                                                        + ((ur != null) ? ur.email : smartGenie.owner.email)
                                                                        + " title: Passive Device Notification "
                                                                        + " content: " + xGenie.name + " " + event.eventMsg + " "
                                                                        + ",Message ID:" + strMsgId);
                                                            } else {
                                                                notificationMap.get(ContactType.EMAIL).pushNotification(
                                                                        (ur != null) ? ur.email : smartGenie.owner.email,
                                                                        "Passive Device Notification",
                                                                        environment + xGenie.name + " " + event.eventMsg);
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment
                                                                        + " AG send " + " email to "
                                                                        + ((ur != null) ? ur.email : smartGenie.owner.email)
                                                                        + " title: Passive Device Notification "
                                                                        + " content: " + xGenie.name + " " + event.eventMsg);
                                                            }
                                                        }
                                                        if (contact.notifyPushNotification) {
                                                            String strDate = strMsgId.substring(6, 10) + "/" +
                                                                    strMsgId.substring(10, 12) + "/" +
                                                                    strMsgId.substring(12, 14) + "T" +
                                                                    strMsgId.substring(14, 16) + ":" +
                                                                    strMsgId.substring(16, 18) + ":" +
                                                                    strMsgId.substring(18, 20);
                                                            SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);
                                                            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");
                                                            df.setTimeZone(TimeZone.getTimeZone("GMT+0"));
                                                            Date date = df.parse(strDate);
                                                            Calendar cal = Calendar.getInstance();
                                                            cal.setTime(date);
                                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                                            if (sgLog.timezone > 0) {
                                                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+"
                                                                        + String.valueOf(sgLog.timezone)));
                                                            } else if (sgLog.timezone < 0) {
                                                                sdf.setTimeZone(TimeZone.getTimeZone("GMT"
                                                                        + String.valueOf(sgLog.timezone)));
                                                            } else {
                                                                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                                            }

                                                            User user = Ebean.find(User.class, contact.userId);

                                                            if (!"".equals(strMsgId)) {
                                                                notificationMap.get(ContactType.PUSH_NOTIFICATION).pushNotification(
                                                                        user.id.toString(),
                                                                        "Passive Device Notification",
                                                                        environment + xGenie.name + " " + event.eventMsg
                                                                                + "(" + sdf.format(cal.getTime()) + ")" + " "
                                                                                + ",Message ID:" + strMsgId);
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                                        environment + " AG send " + " notification to "
                                                                                + user.id.toString() + " title: Passive Device Notification "
                                                                                + " content: " + xGenie.name
                                                                                + " " + event.eventMsg + "(" + sdf.format(cal.getTime())
                                                                                + ")" + " " + ",Message ID:" + strMsgId);
                                                            } else {
                                                                notificationMap.get(ContactType.PUSH_NOTIFICATION).pushNotification(
                                                                        user.id.toString(),
                                                                        "Passive Device Notification",
                                                                        environment + xGenie.name + " " + event.eventMsg
                                                                                + "(" + sdf.format(cal.getTime()) + ")");
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment
                                                                        + " AG send " + " Notification to "
                                                                        + user.id.toString() + " title: Passive Device Notification "
                                                                        + " content: " + xGenie.name + " " + event.eventMsg
                                                                        + "(" + sdf.format(cal.getTime()) + ")");
                                                            }
                                                        }
                                                        if (contact.notifySMS) {
                                                            if (event.eventType == 10002 || event.eventType == 10003 ||
                                                                    event.eventType == 10100 || event.eventType == 10101 ||
                                                                    event.eventType == 10102 || event.eventType == 10103 ||
                                                                    event.eventType == -10150 || event.eventType == -10151) {
                                                                sendEmail2Text(contact.userId,
                                                                        "Passive Device Notification",
                                                                        environment + xGenie.name + " "
                                                                                + event.eventMsg + " "
                                                                                + ",Message ID:" + strMsgId);
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                                        environment + " AG send "
                                                                                + " Email2Text to " + (ur != null ? ur.email : "(no UR)")
                                                                                + " title: Passive Device Notification "
                                                                                + " content: "
                                                                                + (xGenie != null ? xGenie.name : "(no XGenie)") + " "
                                                                                + (event != null ? event.eventMsg : "(no event)")
                                                                                + " " + ",Message ID:" + strMsgId);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            //Asante Genie Event without passive device mac address.
                                            SmartGenie sg = Ebean.find(SmartGenie.class, msg.sgId);

                                            if (sg != null && sg.owner != null) {
                                                User owner = Ebean.find(User.class, sg.owner.id.toString());
                                                //Check Contact -> mac address, event type, and owner
                                                //that notification setting.
                                                EventContact ec = EventContact.findByMacAndEventType(sg.macAddress, event.eventType);

                                                if (event.eventType == 100) {
                                                    String strDate = strMsgId.substring(6, 10) + "/" +
                                                            strMsgId.substring(10, 12) + "/" +
                                                            strMsgId.substring(12, 14) + "T" +
                                                            strMsgId.substring(14, 16) + ":" +
                                                            strMsgId.substring(16, 18) + ":" +
                                                            strMsgId.substring(18, 20);
                                                    SmartGenieLog sgLog = SmartGenieLog.find(sg.id);
                                                    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");
                                                    df.setTimeZone(TimeZone.getTimeZone("GMT+0"));
                                                    Date date = df.parse(strDate);
                                                    Calendar cal = Calendar.getInstance();
                                                    cal.setTime(date);
                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                                    if (sgLog.timezone > 0) {
                                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT+" + String.valueOf(sgLog.timezone)));
                                                    } else if (sgLog.timezone < 0) {
                                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT" + String.valueOf(sgLog.timezone)));
                                                    } else {
                                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                                    }

                                                    notificationMap.get(ContactType.PUSH_NOTIFICATION).pushNotification(owner.id.toString(),
                                                            "Asante Notification", environment + sg.name + " " + event.eventMsg
                                                                    + "(" + sdf.format(cal.getTime()) + ")");
                                                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment + " AG send "
                                                            + " Notification to " + owner.id.toString() + " title: Asante Notification "
                                                            + " content: " + sg.name + " " + event.eventMsg
                                                            + "(" + sdf.format(cal.getTime()) + ")");
                                                } else if (event.eventType != 100 && ec != null) {
                                                    /// Event Type != 100 -> mostly event
                                                    Contact contact = Contact.findByEventContactNUserId(ec.id, owner.id.toString());

                                                    if (contact != null) {
                                                        if (contact.notifySMS) {
                                                            String content;
                                                            if (!"".equals(strMsgId)) {
                                                                content = environment + sg.name + " " + event.eventMsg + " "
                                                                        + ",Message ID:" + strMsgId;
                                                            } else {
                                                                content = environment + sg.name + " " + event.eventMsg;
                                                            }

                                                            sendEmail2Text(contact.userId, "Asante Notification", content);
                                                            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment + " AG sends "
                                                                    + " Email2Text to " + sg.owner.email + " title: Asante Notification "
                                                                    + " content: " + content);
                                                        }

                                                        if (contact.notifyEmail) {
                                                            if (!"".equals(strMsgId)) {
                                                                notificationMap.get(ContactType.EMAIL).pushNotification(owner.email,
                                                                        "Asante Notification", environment + sg.name
                                                                                + " " + event.eventMsg + " " + ",Message ID:" + strMsgId);
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment + " AG send "
                                                                        + " email to " + owner.email + " title: Asante Notification "
                                                                        + " content: " + sg.name + " " + event.eventMsg + " "
                                                                        + ",Message ID:" + strMsgId);
                                                            } else {
                                                                notificationMap.get(ContactType.EMAIL).pushNotification(owner.email,
                                                                        "Asante Notification",
                                                                        environment + sg.name + " " + event.eventMsg);
                                                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, environment
                                                                        + " AG send " + " email to " + owner.email
                                                                        + " title: Asante Notification " + " content: "
                                                                        + sg.name + " " + event.eventMsg);
                                                            }
                                                        }
                                                        if (contact.notifyPushNotification) {
                                                            String strDate = strMsgId.substring(6, 10) + "/" +
                                                                    strMsgId.substring(10, 12) + "/" +
                                                                    strMsgId.substring(12, 14) + "T" +
                                                                    strMsgId.substring(14, 16) + ":" +
                                                                    strMsgId.substring(16, 18) + ":" +
                                                                    strMsgId.substring(18, 20);
                                                            SmartGenieLog sgLog = SmartGenieLog.find(sg.id);
                                                            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");
                                                            df.setTimeZone(TimeZone.getTimeZone("GMT+0"));
                                                            Date date = df.parse(strDate);
                                                            Calendar cal = Calendar.getInstance();
                                                            cal.setTime(date);
                                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                                            if (sgLog.timezone > 0) {
                                                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+" + String.valueOf(sgLog.timezone)));
                                                            } else if (sgLog.timezone < 0) {
                                                                sdf.setTimeZone(TimeZone.getTimeZone("GMT" + String.valueOf(sgLog.timezone)));
                                                            } else {
                                                                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                                            }
                                                            if (contact.notifyEmail) {
                                                                if (!"".equals(strMsgId)) {
                                                                    notificationMap.get(ContactType.PUSH_NOTIFICATION).
                                                                            pushNotification(owner.id.toString(),
                                                                                    "Asante Notification",
                                                                                    environment + sg.name + " " + event.eventMsg
                                                                                            + "(" + sdf.format(cal.getTime()) + ")"
                                                                                            + " " + ",Message ID:" + strMsgId);
                                                                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                                            environment + " AG send " + " notification to "
                                                                                    + owner.id.toString() + " title: Asante Notification "
                                                                                    + " content: " + sg.name + " " + event.eventMsg
                                                                                    + "(" + sdf.format(cal.getTime())
                                                                                    + ")" + " " + ",Message ID:" + strMsgId);
                                                                } else {
                                                                    notificationMap.get(ContactType.PUSH_NOTIFICATION).
                                                                            pushNotification(owner.id.toString(),
                                                                                    "Asante Notification",
                                                                                    environment + sg.name + " " + event.eventMsg
                                                                                            + "(" + sdf.format(cal.getTime()) + ")");
                                                                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                                            environment + " AG send "
                                                                                    + " notification to " + owner.id.toString()
                                                                                    + " title: Asante Notification "
                                                                                    + " content: " + sg.name + " " + event.eventMsg
                                                                                    + "(" + sdf.format(cal.getTime()) + ")");
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "event contact: " + ec.id
                                                                + " owner id: " + owner.id + "does not have contact");
                                                    }
                                                } else {
                                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "sg macAddress: "
                                                            + sg.macAddress + " and eventType: " + event.eventType
                                                            + " does not have event contact" + " and eventType != 100");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    for (int i = 0; i < msg.listOwnerEmail.size(); i++) {
                                        if (msg.isEmail) {
                                            notificationMap.get(ContactType.EMAIL).pushNotification(
                                                    msg.listOwnerEmail.get(i),
                                                    msg.title,
                                                    environment + msg.content);
                                            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                    environment + " nonAG send " + " email to "
                                                            + msg.listOwnerEmail.get(i) + " title: " + msg.title
                                                            + " content: " + msg.content);
                                        }
                                        if (msg.isPushNotification) {
                                            User user = User.findByEmail(msg.listOwnerEmail.get(i));

                                            notificationMap.get(ContactType.PUSH_NOTIFICATION).pushNotification(
                                                    user.id.toString(),
                                                    msg.title,
                                                    environment + msg.content);
                                            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                    environment + " nonAG send "
                                                            + " notification to " + user.id.toString()
                                                            + " title: " + msg.title + " content: " + msg.content);
                                        }
                                    }
                                }

                                sqsManager.deleteMessage(queueUrl, message);
                            } catch (Exception e) {
                                try {
                                    sqsManager.deleteMessage(queueUrl, message);
                                    e.printStackTrace();
                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                            "SQS Manual message error : "
                                                    + ExceptionUtil.exceptionStacktraceToString(e));
                                } catch (InvalidIdFormatException err) {
                                    err.printStackTrace();
                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                            "sqsoffice delete Invalid Id Format Exception "
                                                    + ExceptionUtil.exceptionStacktraceToString(err));
                                } catch (ReceiptHandleIsInvalidException err) {
                                    err.printStackTrace();
                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                            "sqsoffice delete Receipt Handle Is Invalid Exception "
                                                    + ExceptionUtil.exceptionStacktraceToString(err));
                                } catch (Exception err) {
                                    err.printStackTrace();
                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "delete SQS message error : "
                                            + ExceptionUtil.exceptionStacktraceToString(err));
                                }
                            }
                        }
                    }
                    Calendar end_time = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                            "consumeMsgQueue ends at" + " " + sdf.format(end_time.getTime()));
                }
                , actorSystem.dispatcher()
        );
    }

    public void MonitorSmartGenie() {
        actorSystem.scheduler().schedule(Duration.create(Constants.PING_TIMER / 1000, TimeUnit.SECONDS), Duration.create(Constants.PING_TIMER / 1000, TimeUnit.SECONDS), () ->
                {
                    Calendar start_time = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "MonitorSmartGenie starts at" + sdf.format(start_time.getTime()));
                    List<String> sgIdList = SmartGenieLog.findSgIdList();
                    Calendar rightNow = Calendar.getInstance();
                    long zero = 0;
                    for (String sgId : sgIdList) {
                        SmartGenieLog sgLog = SmartGenieLog.find(sgId);
                        try {
                            if (sgLog.logDateTime != null && (rightNow.getTimeInMillis() - sgLog.logDateTime.getTimeInMillis()) > 2 * Constants.PING_TIMER && (sgLog.logDateTime.getTimeInMillis() - sgLog.recordDateTime.getTimeInMillis() == zero)) {
                                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "The SmartGenie( ID : " + sgLog.id + " ) DOES NOT WORK!!! at " + sdf.format(rightNow.getTime()));
                                sgLog.recordDateTime = rightNow;
                                sgLog.save();
                                DeviceEventLoggerController.sendMsg(null, sgLog.id, EventLogType.ASANTE_GENIE_NOT_WORK.value, rightNow, sdf.format(rightNow.getTime()));
                                //  LoggingUtils.log(LoggingUtils.LogLevel.INFO, "record date time after save " + sdf.format(sgLog.recordDateTime.getTime()) + "log date time after save " + sdf.format(sgLog.logDateTime.getTime()) );
                            }
                        } catch (Exception err) {
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "MonitorSmartGenie error" + ExceptionUtil.exceptionStacktraceToString(err));
                        }

                    }
                    Calendar end_time = Calendar.getInstance();
                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, " MonitorSmartGenie ends at " + sdf.format(end_time.getTime()));
                }
                , actorSystem.dispatcher()
        );
    }

    public void MonitorEc2RdsStorage() {
        Integer schedule_period = configuration.getInt("event.schedule.period");
        Integer schedule_init = configuration.getInt("event.schedule.init");
        final Integer before_data = configuration.getInt("delete.before");
        actorSystem.scheduler().schedule(Duration.create(schedule_init, TimeUnit.SECONDS), Duration.create(schedule_period, TimeUnit.SECONDS), () ->
                {
                    try {
                        //Remove debug log from RDS
                        Calendar start_time = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                "MonitorEc2RdsStorage starts at" + sdf.format(start_time.getTime()));
                        Calendar before_Days = Calendar.getInstance();
                        int[] time;
                        time = count_times(before_data);
                        if (time[0] != 0) {
                            before_Days.add(Calendar.DATE, -time[0]);
                        }
                        if (time[1] != 0) {
                            before_Days.add(Calendar.HOUR, -time[1]);
                        }
                        if (time[2] != 0) {
                            before_Days.add(Calendar.MINUTE, -time[2]);
                        }
                        if (time[3] != 0) {
                            before_Days.add(Calendar.SECOND, -time[3]);
                        }
                        LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                "before : " + sdf.format(before_Days.getTime()) + " be deleted");
                        String strQuery = "delete from smart_genie_action_history where create_at < '"
                                + sdf.format(before_Days.getTime()) + "';";
                        LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "strQuery : " + strQuery);

                        SqlUpdate delete = Ebean.createSqlUpdate(strQuery);

                        delete.execute();
                        Calendar end_time = Calendar.getInstance();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "MonitorEc2RdsStorage ends at"
                                + sdf.format(end_time.getTime()));

                    } catch (Exception err) {
                        err.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "MonitorEc2RdsStorage->err : "
                                + ExceptionUtil.exceptionStacktraceToString(err));
                    }
                }
                , actorSystem.dispatcher()
        );
    }

    public void sqlEventScheduler() {
        Integer schedule_period = ConfigUtil.getEventSchedulePeriod();
        Integer schedule_init = ConfigUtil.getEventScheduleInit();
        final Integer before_data = ConfigUtil.getDeleteBefore();
        try {
            int[] time;
            time = count_times(before_data);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar before_Days = Calendar.getInstance();
            if (time[0] != 0) {
                before_Days.add(Calendar.DATE, -time[0]);
            }
            if (time[1] != 0) {
                before_Days.add(Calendar.HOUR, -time[1]);
            }
            if (time[2] != 0) {
                before_Days.add(Calendar.MINUTE, -time[2]);
            }
            if (time[3] != 0) {
                before_Days.add(Calendar.SECOND, -time[3]);
            }
            LoggingUtils.log(LoggingUtils.LogLevel.INFO,
                    "before_Days : " + sdf.format(before_Days.getTime()));
            String strQuery = "CREATE EVENT IF NOT EXISTS delete_smart_genie_action" +
                    " ON SCHEDULE EVERY " + schedule_period.toString() + " SECOND " +
                    " STARTS CURRENT_TIMESTAMP + INTERVAL " + schedule_init.toString() + " SECOND" +
                    " DO DELETE FROM smart_genie_action_history WHERE CREATE_AT < '" +
                    sdf.format(before_Days.getTime()) + "'; ";
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "strQuery : " + strQuery);
            SqlUpdate delete = Ebean.createSqlUpdate(strQuery);
            delete.execute();
            String strQuery_start = "SET GLOBAL event_scheduler = ON;";
            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "strQuery : " + strQuery_start);
            SqlUpdate event_start = Ebean.createSqlUpdate(strQuery_start);
            event_start.execute();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "event scheduler "
                    + ExceptionUtil.exceptionStacktraceToString(err));
        }
    }

    private int[] count_times(Integer remain_seconds) {
        int[] ar = new int[4];
        int days = (remain_seconds / 86400);
        int hours = (remain_seconds - days * 86400) / 3600;
        int minutes = (remain_seconds - days * 86400 - hours * 3600) / 60;
        int seconds = (remain_seconds - days * 86400 - hours * 3600 - minutes * 60);
        ar[0] = days;
        ar[1] = hours;
        ar[2] = minutes;
        ar[3] = seconds;
        return ar;
    }

    private static void sendEmail2Text(final String userId,
                                       final String title, final String content) {
        final String TAG = "sendEmail2Text: ";

        if (title == null || content == null) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "title or content is null");
            return;
        }

        User user = User.find(userId);
        if (user == null) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "no such user: " + userId);
            return;
        }

        if (user.gateway == null || user.cellPhone == null) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "no setup: " + userId);
            return;
        }

        String emailAddress = Email2TextController.getEmail2Text(user.cellPhone, user.gateway);

        if (StringUtils.isBlank(emailAddress)) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "invalid email address");
            return;
        }

        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + emailAddress);
        MailUtil.instance().sendMail(emailAddress, title, content);
    }
}
