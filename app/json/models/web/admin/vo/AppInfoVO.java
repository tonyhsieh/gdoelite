/**
 * 
 */
package json.models.web.admin.vo;

import models.AppInfo;
import models.FirmwareGroup;

/**
 * @author johnwu
 * @version 創建時間：2013/10/3 上午10:48:53
 */

public class AppInfoVO extends BaseVO {

	/**
	 *
	 */
	private static final long serialVersionUID = -3493080282306533134L;

	public String id;
	public String version_name;
	public String device_os_name;
	public int version_code;
	public int force_upgrade;


	public AppInfoVO(AppInfo appInfo) {
		super(appInfo);

		this.id = appInfo.id.toString();
		this.device_os_name = appInfo.device_os_name;
		this.version_name = appInfo.version_name;
		this.version_code = appInfo.version_code;
		this.force_upgrade = appInfo.force_upgrade;
		
	}
}
