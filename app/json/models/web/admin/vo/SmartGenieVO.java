/**
 *
 */
package json.models.web.admin.vo;

import java.util.ArrayList;
import java.util.List;

import models.SmartGenie;
import models.SmartGenieLog;
import models.XGenie;

/**
 * @author carloxwang
 *
 */
public class SmartGenieVO extends BaseVO {

	/**
	 *
	 */
	private static final long serialVersionUID = 90252769323184841L;
	public String smartGenieId;
	public String name;
	public String macAddr;
	public String status;
	public UserVO owner;
	public List<XGenieVO> xGenies;
	
	public String extIP;
	public String innIP;
	public String wifiMAC;
	public SmartGenieInfoVO sgInfo;
	public String firmwareVer;
	
	public String wifiIP;
	public String ssidName;
	public int p2pSuccess;
	public int p2pFail;

	public SmartGenieVO(SmartGenie sg, List<XGenie> xGenies) {
		super(sg);

		this.smartGenieId = sg.id.toString();
		this.name = sg.name;
		this.macAddr = sg.macAddress;
		this.status = new String();
		
		if(sg.owner != null){
			this.owner = new UserVO(sg.owner);
		}else{
			this.owner = new UserVO(null);
		}

		if(xGenies != null){
			this.xGenies = new ArrayList<XGenieVO>(xGenies.size());

			for(XGenie xg : xGenies){
				this.xGenies.add(new XGenieVO(xg));
			}
		}

	}

	public SmartGenieVO(SmartGenie sg, SmartGenieLog sgLog, SmartGenieInfoVO sgInfo){
		super(sg);
		
		this.smartGenieId = sg.id.toString();
		this.name = sg.name;
		this.macAddr = sg.macAddress;
		//this.firmwareVer = sg.firmwareGroup.firmwareVersion;
		
		this.extIP = sgLog.extIp;
		this.innIP = sgLog.innIp;
		this.wifiMAC = sgLog.wifiMacAddress;
		
		this.sgInfo = sgInfo;
		
		this.firmwareVer = sgLog.firmwareVer;
		this.wifiIP = sgLog.wifiIP;
		this.ssidName = sgLog.ssidName;
		this.p2pSuccess = sgLog.p2pSuccess;
		this.p2pFail = sgLog.p2pFail;
		
	}
}
