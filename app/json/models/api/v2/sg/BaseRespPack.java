/**
 *
 */
package json.models.api.v2.sg;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json.models.api.v1.sg.CalendarDeserializer;
import json.models.api.v1.sg.CalendarSerializer;
import json.models.api.v1.sg.JsonStatusCode;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author carloxwang
 *
 */
@JsonAutoDetect( fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE )
public class BaseRespPack implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6530855634561555817L;

	public JsonStatusCode status = new JsonStatusCode();

	@JsonSerialize(using=CalendarSerializer.class)
	@JsonDeserialize(using=CalendarDeserializer.class)
	public Calendar sysTimestamp;

}
