package json.models.api.v2.sg;

/**
 * 
 * @author johnwu
 *
 */
public class SmartGenieRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6959466290295491685L;

	public String sessionId;
	
	public String homeExtenderId;
	
	public String p2p_activecode;

    public String s3_buckets;

	public int timezone;

    public Boolean autoUpgrade;

    public String autoUpgradeTime;

	public String autoUpgradeOnceTime;

    public Boolean autoUpgradeOnce;

}
