package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.ConfigurationInfoBaseVO;

import java.util.List;

public class SwitchConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 77866802053595934L;

	public List<? super ConfigurationInfoBaseVO> configlist;
}
