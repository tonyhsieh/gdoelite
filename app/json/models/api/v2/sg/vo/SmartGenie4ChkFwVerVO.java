package json.models.api.v2.sg.vo;

public class SmartGenie4ChkFwVerVO {

	public String newFwVer;

	public int forceUpgrade;

	public String downloadUrl;

	public String forceFwVer;
	
	public String downloadForceUrl;

}

