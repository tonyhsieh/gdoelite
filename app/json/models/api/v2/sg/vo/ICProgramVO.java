package json.models.api.v2.sg.vo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect( fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE )
public class ICProgramVO {
    
    public String desc;
    
    public String xGenieMac;
    
    public boolean available;
    
    public String cfgType;
    
    public String cfgId;
    
    public String configuration;
}
