package json.models.api.v1.app;

import json.models.api.v1.app.vo.SmartGenieNetworkInfoVO;

public class SmartGenieNetworkRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String sessionId;
	
	public SmartGenieNetworkInfoVO networkInfo;
}
