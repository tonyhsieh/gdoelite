package json.models.api.v1.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import json.models.api.v1.app.vo.ContactMemberVO;
import models.Contact;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ContactMemberRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3104271811037913293L;

	public String sessionId;
	
	public ContactMemberRespPack(){}
	
	public List<ContactMemberVO> listContact = new ArrayList<ContactMemberVO>();
	
	public void getListContactMember(Contact contactMember){
		
		ContactMemberVO contactMemberVO = new ContactMemberVO();

//		contactMemberVO.name = contactMember.contactName;
//		contactMemberVO.contactType = contactMember.contactType;
//		contactMemberVO.contactData = contactMember.contactData;
//		contactMemberVO.relationship = contactMember.relationship;
		contactMemberVO.contactId = contactMember.id.toString();
		
		listContact.add(contactMemberVO);
		
		ObjectMapper om = new ObjectMapper();
		String responseString = "";
		
		try {
			responseString = om.writeValueAsString(listContact);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
