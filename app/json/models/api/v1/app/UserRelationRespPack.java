/**
 *
 */
package json.models.api.v1.app;

import json.models.api.v1.app.vo.PageVO;
import json.models.api.v1.app.vo.UserVO;

/**
 * @author carloxwang
 *
 */
public class UserRelationRespPack extends BaseRespPack {

	/**
	 *
	 */
	private static final long serialVersionUID = -3973883944563415988L;

	public PageVO<UserVO> userList;
}
