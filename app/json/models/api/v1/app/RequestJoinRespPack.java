package json.models.api.v1.app;

import java.util.List;

import json.models.api.v1.app.vo.RequestJoinUserVO;

/**
 * 
 * @author johnwu
 *
 */

public class RequestJoinRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7799438578937685227L;

	public String sessionId;
	
	public List<RequestJoinUserVO> listUser;
}
