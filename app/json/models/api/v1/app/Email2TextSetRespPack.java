package json.models.api.v1.app;

public class Email2TextSetRespPack extends BaseRespPack
{
    private static final long serialVersionUID = -1984033489582866191L;

    public String smsEmail;
    public String pin;
    public Long waitingTime;

    @Override
    public String toString()
    {
        return "Email2TextSetRespPack{" +
                "smsEmail='" + smsEmail + '\'' +
                ", pin='" + pin + '\'' +
                ", waitingTime=" + waitingTime +
                '}';
    }
}
