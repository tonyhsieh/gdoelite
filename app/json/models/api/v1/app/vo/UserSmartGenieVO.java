package json.models.api.v1.app.vo;

public class UserSmartGenieVO{

	public String smartGenieId;
	public String smartGenieName;
	public Integer smartGenieStatusCode;
	public Integer userType;
	public String macAddr;
	public Integer isALive;
	public String lat;
	public String lng;
}
