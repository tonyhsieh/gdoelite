/**
 *
 */
package json.models.api.v1.app.vo;

import java.io.Serializable;
import java.util.Calendar;

import json.models.api.v1.app.CalendarSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author carloxwang
 *
 */
public class BaseVO implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = -9185599038085543664L;

	/**
     * 記錄本筆資料建立時間
     */
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar createAt;

    /**
     * 記錄本筆資料最後一次更新時間
     */
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar updateAt;

}
