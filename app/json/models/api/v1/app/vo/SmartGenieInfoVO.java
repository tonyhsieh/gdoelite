package json.models.api.v1.app.vo;

public class SmartGenieInfoVO {
	public String smartGenieId;
	public String smartGenieName;
	public Integer smartGenieStatusCode;
	public Integer userType;
	public String macAddr;
	public Integer isALive;
	public boolean isAtLan;
	public String smartGenieInnerIP;
	public String smartGenieNetmask;
	public String wifiMacAddress;
	public String fwVer;
	public Integer devType;
	public String ownerId;
	public String lat;
	public String lng;
	public String photoURL;
}
