package json.models.api.v1.app.vo;

import java.io.Serializable;
import java.util.List;

public class EventContactVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5121411818806095234L;
	
	public List<EventContactData> ecDataList;
	
}
