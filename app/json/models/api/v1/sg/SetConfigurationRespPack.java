package json.models.api.v1.sg;

public class SetConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 849538828945262738L;

	public String sessionId;
	
	public String configurationId;
}
