package json.models.api.v1.sg;

public class GetSgLocation extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1678119932270643782L;

	public String sessionId;
	
	public String lat;
	
	public String lng;
}
