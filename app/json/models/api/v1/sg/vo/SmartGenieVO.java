package json.models.api.v1.sg.vo;

/**
 * 
 * @author johnwu
 *
 */

public class SmartGenieVO{

	public String newFwVer;

	public String downloadUrl;
	public int forceUpgrade;

	public String forceFwVer;
	public String downloadForceUrl;


}
