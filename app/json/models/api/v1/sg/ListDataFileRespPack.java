package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.DataFileVO;
import json.models.api.v1.sg.vo.RemoveDeviceVO;

public class ListDataFileRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5159720975796209721L;

	public String sessionId;
	
	public List<DataFileVO> listFile;
	
}
