package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.app.vo.UserSmartGenieVO;
import json.models.api.v1.sg.vo.UserInfoVO;

public class ListUserInfoBySgSessionIdRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7297111453683001133L;
	
	public String sessionId;
	
	public List<UserInfoVO> listUserInfoVO;
}
