package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.RemoveDeviceVO;
import json.models.api.v1.sg.vo.UserInfoVO;

public class ListRemoveDeviceRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5042750073280983892L;

	public String sessionId;
	
	public List<RemoveDeviceVO> result;
}
