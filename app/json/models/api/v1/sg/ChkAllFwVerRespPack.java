package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.SmartGenie4ChkFwVerVO;
import json.models.api.v1.sg.vo.XGenie4ChkFwVerVO;

public class ChkAllFwVerRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2270608738187634109L;

	public String sessionId;
	
	public SmartGenie4ChkFwVerVO smartGenie;
	
	public List<XGenie4ChkFwVerVO> xgenie;
}
