package json.models.api.v1.sg;

public class DownloadDataFileRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2354385854633874249L;

	public String sessionId;
	
	public String contentType;
	
	public String result;
}
