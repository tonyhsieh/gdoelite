/**
 *
 */
package json.models.api.v1.sg;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import frameworks.Constants;

/**
 * @author carloxwang
 *
 */
public class CalendarSerializer extends JsonSerializer<Calendar> {

	public void serialize(Calendar calendar, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.JSON_CALENDAR_PATTERN);

        jsonGenerator.writeString(dateFormat.format(calendar.getTime()));

	}

}
