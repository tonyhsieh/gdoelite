package utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by tony_hsieh on 2016/12/14.
 */
public class ExceptionUtil
{
    public static String exceptionStacktraceToString(Exception e)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        e.printStackTrace(ps);
        ps.close();
        return baos.toString();
    }
}
