package utils;

import play.Configuration;

/**
 * wrapper for Play Framework's configuration
 */
public class ConfigUtil
{
    private static Configuration getConfig()
    {
        return play.api.Play.current().injector().instanceOf(play.Application.class).configuration();
    }

    public static String getTimeZone()
    {
        return getConfig().getString("timezone");
    }

    public static Boolean getLogEnable()
    {
        return getConfig().getBoolean("log.enable");
    }

    public static String getApplicationEnvironment()
    {
        return getConfig().getString("application.environment");
    }

    public static String getQueueRegion()
    {
        return getConfig().getString("queue.region");
    }

    public static int getEventSchedulePeriod()
    {
        return getConfig().getInt("event.schedule.period");
    }

    public static int getEventScheduleInit()
    {
        return getConfig().getInt("event.schedule.init");
    }

    public static int getDeleteBefore()
    {
        return getConfig().getInt("delete.before");
    }

    public static int getIcScheduleHour()
    {
        return getConfig().getInt("ic.scheduler.hour");
    }

    public static int getIcSchedulerMinute()
    {
        return getConfig().getInt("ic.scheduler.minute");
    }

    public static Boolean getApplicationNotification()
    {
        return getConfig().getBoolean("application.notification");
    }

    public static String getQueueName1()
    {
        return getConfig().getString("queue.name.1");
    }

    public static String getQueueAttrDelaySeconds()
    {
        return getConfig().getString("queue.attr.DelaySeconds");
    }

    public static String getQueueAttrMaximumMessageSize()
    {
        return getConfig().getString("queue.attr.MaximumMessageSize");
    }

    public static String getQueueAttrMessageRetentionPeriod()
    {
        return getConfig().getString("queue.attr.MessageRetentionPeriod");
    }

    public static String getQueueAttrReceiveMessageWaitTimeSeconds()
    {
        return getConfig().getString("queue.attr.ReceiveMessageWaitTimeSeconds");
    }

    public static String getQueueAttrVisibilityTimeOut()
    {
        return getConfig().getString("queue.attr.VisibilityTimeout");
    }

    public static int getReceiveMessageMaxNumberOfMessages()
    {
        return getConfig().getInt("queue.receive.message.maxNumberOfMessages");
    }

    public static int getQueueReceiveMessageWaitTimeSeconds()
    {
        return getConfig().getInt("queue.receive.message.waitTimeSeconds");
    }

    public static String getAccessKey()
    {
        return getConfig().getString("accessKey");
    }

    public static String getSecretKey()
    {
        return getConfig().getString("secretKey");
    }

    public static String getMaxMindPath()
    {
        return getConfig().getString("maxmind.path");
    }

    public static String getS3SgFirmware()
    {
        return getConfig().getString("s3.sg.firmware");
    }

    public static String getS3XgFirmware()
    {
        return getConfig().getString("s3.xg.firmware");
    }

    public static String getS3EventLogAttachment()
    {
        return getConfig().getString("s3.event.log.attachment");
    }

    public static String getS3SgConfiguration()
    {
        return getConfig().getString("s3.sg.configuration");
    }

    public static String getS3SgPhoto()
    {
        return getConfig().getString("s3.sg.photo");
    }

    public static String getS3ActiveCode()
    {
        return getConfig().getString("s3.activecode");
    }

    public static String getS3ConfigurationIrrigationController()
    {
        return getConfig().getString("s3.configuration.irrigation.controller");
    }

    public static String getS3Region()
    {
        return getConfig().getString("s3.region");
    }

    public static String getS3BucketsHttpUrl()
    {
        return getConfig().getString("s3.buckets.httpurl");
    }

    public static String getS3BucketsUrl()
    {
        return getConfig().getString("s3.buckets.url");
    }


    public static String getLogRemoteHttpUrl()
    {
        return getConfig().getString("log.remote.httpUrl");
    }

    public static String getLogRemoteHttpUser()
    {
        return getConfig().getString("log.remote.httpUser");
    }

    public static String getLogRemoteHttpPassword()
    {
        return getConfig().getString("log.remote.httpPassword");
    }

    public static String getTransporterUrl()
    {
        return getConfig().getString("transporter.httpUrl");
    }

    public static boolean isAPNSProduction()
    {
        return getConfig().getBoolean("apns.environment", false);
    }

    public static int getVersionCodeForIosUnder8()
    {
        return getConfig().getInt("versionCode_for_ios_under8");
    }

    public static String getVersionNameIosUnder8()
    {
        return getConfig().getString("versionName_ios_under8");
    }

    public static int getForceUpgradeIosUnder8()
    {
        return getConfig().getInt("forceupgrade_ios_under8");
    }

    public static String getIftttChannelKey()
    {
        return getConfig().getString("ifttt.channel.key");
    }

    public static String getOauth2ServerEndPoint()
    {
        return getConfig().getString("oauth2.server.endpoint");
    }

    public static String getEmailSender()
    {
        return getConfig().getString("email.sender");
    }

    public static String getEmail2textAPI()
    {
        return getConfig().getString("email2text.validator.api.url");
    }

    public static String getEmail2textAPIKey()
    {
        return getConfig().getString("email2text.validator.api.key");
    }


    public static String getP2pServerInternalIP()
    {
        return getConfig().getString("p2p.server.internal.ip");
    }

    public static int getP2pSocketPort()
    {
        return getConfig().getInt("p2p.socket.port");
    }

    public static int getP2pSessionKeyTtlDays()
    {
        return getConfig().getInt("p2p.session.key.ttl.days");
    }













}
