/**
 * 
 */
package utils.sendhub.json.vo.req;

import java.io.Serializable;

/**
 * @author johnwu
 * @version 創建時間：2013/8/28 上午11:17:13
 */

public class EditContactVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7269293380513770552L;

	public String id;
	
	public String name;
	
	public String number;
}
