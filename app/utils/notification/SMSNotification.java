package utils.notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.LoggingUtils;
import utils.sendhub.SendHubUtils;

/**
 * @author johnwu
 * @version 創建時間：2013/8/15 下午4:19:11
 */
public class SMSNotification implements Notification
{
    public void pushNotification(String to, String title, String content)
            throws IOException
    {
        List<Long> contacts = new ArrayList<>(1);
        contacts.add(Long.parseLong(to));

        if (!SendHubUtils.sendMessage(contacts, content))
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "error sending " + content);
        }
    }

    @Override
    public void getPushNotification()
    {
    }
}
