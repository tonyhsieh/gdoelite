package utils.notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javapns.notification.ResponsePacket;
import models.PnRelationship;
import models.User;
import utils.GCMUtil;
import utils.JavaPNSUtils;
import utils.LoggingUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.avaje.ebean.Ebean;
import play.Configuration;
import javax.inject.Inject;
/**
 * 
 * @author Jack
 *
 */

public class GCMNotification implements Notification{

    @Inject static Configuration configuration;
	private GCMUtil gcmUtil = GCMUtil.getInstance();
	
	@Override
	public void pushNotification(String userId, String title, String content) throws IOException {
		// TODO Auto-generated method stub
		List<String> receivers = new ArrayList<String>();
		
		User user = Ebean.find(User.class, userId);

		/////////////////////////////////////////////////////////////////////////////////
		//For Android Device
		List<PnRelationship> pnList = PnRelationship.listByUserAndOs(user, "android");
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "android pnlist size " + pnList.size());
		for(PnRelationship pnR:pnList){
			receivers.add(pnR.pnId);
		}
		
		if(receivers.size() > 0){
			gcmUtil.send(receivers, title, content);
		}
		
		//////////////////////////////////////////////////////////////////////////////////
		//For iOS Device
		//Add by Jack 2013/12/30
		
		ResponsePacket response = null;
		
		JavaPNSUtils apns = new JavaPNSUtils();
		
		pnList = PnRelationship.listByUserAndOs(user, "ios");
		
		receivers = new ArrayList<String>();
		for(PnRelationship pnR:pnList){
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "pushNotification send to reg email : " + pnR.user.email);
		    //Logger.info("pushNotification send to reg email : " + pnR.user.email);
			receivers.add(pnR.pnId);
		}
		
		if(receivers.size() > 0){
			LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "ios receivers list  : " + receivers + "receivers size " + receivers.size());
			for(String pnId: receivers){
                response = apns.push(content, 1, "default", pnId);
                if (response != null){
                    LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "apns resp status : " + response.getStatus() + " / msg : " + response.getMessage());
                }


				//Logger.info("apns resp status : " + response.getStatus() + " / msg : " + response.getMessage());
			}
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////
		//For Amazon
		
		List<String> admReceivers = new ArrayList<String>();
		pnList = PnRelationship.listByUserAndOs(user, "amazon");
		
		/*
		AmazonSNS sns = null;
		
		String accessKey = "AKIAJSOTSXC7JNMQQVFQ";
        String secretKey = "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";
		
		AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
		sns = new AmazonSNSClient(awsCredentials);

		Region usWest2 = Region.getRegion(Regions.US_EAST_1);
		sns.setRegion(usWest2);
		*/
		
		
		ADMNotification adm = null;
		
		for(PnRelationship pnR:pnList){
			admReceivers.add(pnR.pnId);	
		}
		
		if(admReceivers.size() > 0){
			for(String pnId: admReceivers){
				try {
					
						adm = new ADMNotification(
							configuration.getString("adm.clientId"),
							configuration.getString("adm.clientSecret"),
							pnId,
							title, 
							content
							);
					
		
				} catch (AmazonServiceException ase) {
					LoggingUtils.log(LoggingUtils.LogLevel.INFO, "AmazonServiceException : " + ase.toString());
		
		
				} catch (AmazonClientException ace) {
					LoggingUtils.log(LoggingUtils.LogLevel.INFO, "AmazonClientException : " + ace.toString());
		
				}catch(Exception err){
					LoggingUtils.log(LoggingUtils.LogLevel.INFO, "ADM err : " + err.toString());
				}
			}
		}
		//end amazon push notification
		/////////////////////////////////////////////////////////
	}

	@Override
	public void getPushNotification() {
		// TODO Auto-generated method stub
		
	}

}
