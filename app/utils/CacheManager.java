package utils;


import frameworks.models.CacheKey;
import play.api.Play;
import play.cache.CacheApi;

/**
 * Created with IntelliJ IDEA.
 * User: carloxwang
 * Date: 13/4/24
 * Time: PM4:27
 */
public class CacheManager
{

    private static CacheApi getCacheInstance()
    {
        return Play.current().injector().instanceOf(CacheApi.class);
    }
    public static final int USERSESSION_EXPIRED_TIME = 2*24*60*60*1;
	public static final int SGSESSION_EXPIRED_TIME = 2*24*60*60*1;
	public static final int EVENT_ID_EXPIRED_TIME = 1*2*60*60*1;
	public static final int EVENT_BY_MANUAL_ID_EXPIRED_TIME = 1*1*1*60*1;
	public static final int AG_PUBLIC_IP_EXPIRED_TIME = 2*24*60*60*1;
	
	public static final int DB_CACHE_EXPIRED_TIME = 2*24*60*60*1;
	
	public static final String DB_CACHE_VERSION = "v0.1";
	
    public static void remove(CacheKey key){
        removeWithId(key, null);
    }

    public static void removeWithId(CacheKey key, String id){
        getCacheInstance().remove(key.value + id);
    }

    public static void set(CacheKey key, Object value){
        setWithId(key, null, value);
    }

    public static void set(CacheKey key, Object value, int expire){
        setWithId(key, null, value, expire);
    }

    public static void setWithId(CacheKey key, String id, Object value){
        getCacheInstance().set(key.value + id, value);
    }

    public static void setWithId(CacheKey key, String id, Object value, int expire){
        getCacheInstance().set(key.value + id, value, expire);
    }

    public static <T> T get(CacheKey key){
        return get(key, null);
    }

    @SuppressWarnings("unchecked")
	public static <T> T get(CacheKey key, String id){
        return (T) getCacheInstance().get(key.value + id);
    }
    
    //Add By Jack
    public static void setWithVer(String key, Object value, int expire){
        getCacheInstance().set(key, value, expire);
    }
    
    public static void removeWithVer(String key){
        getCacheInstance().remove(key);
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T getWithVer(String key){
        return (T) getCacheInstance().get(key);
    }
    //end
}
