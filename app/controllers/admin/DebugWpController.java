package controllers.admin;

import static play.data.Form.form;
import java.util.ArrayList;
import java.util.List;


import models.Checkout;

import play.data.DynamicForm;
import play.mvc.Result;

import views.html.admin.admin_wpdemo_list;

import com.avaje.ebean.Ebean;

import controllers.BaseController;

public class DebugWpController extends BaseController{

	public Result list(){
		
		List<Checkout> listCheckout = Checkout.findAll();
		
		return ok(admin_wpdemo_list.render(listCheckout));
	}





}
