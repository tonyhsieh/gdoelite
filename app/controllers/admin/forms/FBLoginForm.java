/**
 *
 */
package controllers.admin.forms;

import play.data.validation.Constraints;

/**
 * @author carloxwang
 *
 */
public class FBLoginForm {

	@Constraints.Required
	public String fbUserId;

	@Constraints.Required
	public String accessToken;

	public String from;

	@Override
	public String toString() {
		return "FBLoginForm ["
				+ (fbUserId != null ? "fbUserId=" + fbUserId + ", " : "")
				+ (accessToken != null ? "accessToken=" + accessToken + ", "
						: "") + (from != null ? "from=" + from : "") + "]";
	}


}
