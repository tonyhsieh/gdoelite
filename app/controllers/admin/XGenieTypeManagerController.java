package controllers.admin;

import java.util.List;

import models.XGenieType;
import play.data.DynamicForm;
import play.mvc.Result;
import views.html.admin.xgenie_type_ajax;
import views.html.admin.xgenie_type_manager;

import play.data.FormFactory;
import javax.inject.Inject;

import com.avaje.ebean.Ebean;

import controllers.BaseController;

public class XGenieTypeManagerController extends BaseController{
    @Inject FormFactory formFactory;

	public Result index() {
        return ok(xgenie_type_manager.render());
    }
	
	public Result addNewXGenieType(){

        DynamicForm form = formFactory.form().bindFromRequest();
		
		int xgenieType = Integer.parseInt(form.get("xgenieType"));
		String xgenieName = form.get("xgenieName");
		
		XGenieType xGenieType = Ebean.find(XGenieType.class, xgenieType);
		
		if(xGenieType == null){
			xGenieType = new XGenieType();
			xGenieType.deviceType = xgenieType;
		}
		
		xGenieType.deviceName = xgenieName;
		
		xGenieType.save();
		
		return redirect(controllers.admin.routes.XGenieTypeManagerController.index());
	}
	
	public Result listXGenieTypeAjax(){
		List<XGenieType> listXGenieType = XGenieType.list();
		
		return ok(xgenie_type_ajax.render(listXGenieType));
	}
}
