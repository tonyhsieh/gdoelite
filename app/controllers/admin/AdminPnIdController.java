package controllers.admin;

import controllers.BaseController;
import controllers.admin.forms.MobileInfo;
import json.models.api.v1.app.vo.SmartGenieUserVO;
import json.models.web.admin.vo.P2pActiveCodeVO;
import json.models.web.admin.vo.PageVO;
import json.models.web.admin.vo.PnRelationshipVO;
import models.PnRelationship;
import models.User;
import models.UserDevLog;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.DynamicForm;
import play.mvc.Result;
import utils.LoggingUtils;
import views.html.admin.admin_user_pnidcheck;

import java.text.SimpleDateFormat;
import java.util.*;

import static play.data.Form.form;

/**
 * Created by admin on 2015/11/5.
 */
public class AdminPnIdController extends BaseController {

	/*
    private final static Map<String, String> orderByPropMap = new HashMap<String, String>();
    static {
        orderByPropMap.put("create", "createAt");
        orderByPropMap.put("update", "updateAt");
        orderByPropMap.put("pnid", "pnidAt");
        orderByPropMap.put("devid", "devidAt");
    }
    */

    //
    /*
    public Result listAllPnidAndDevId(String page) {

        DynamicForm df =formFactory.form().bindFromRequest();
        String order = df.get("order");
        String direction = df.get("direction");
        String pnid = df.get("pnid");

        String orderByProp = orderByPropMap.get(order);

        if (orderByProp == null) {
            orderByProp = "createAt";
        }

        Page<PnRelationship> pnidPage = null;

        if (StringUtils.isBlank(pnid)) {
            pnid = null;
            pnidPage = PnRelationship.pnPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
        } else {
            pnidPage = PnRelationship.pnPageWithPnId(pnid.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
        }

        //
        P2pActiveCodeVO p2pVo = new P2pActiveCodeVO(null);

		if(pnidPage.getList().isEmpty()){
			P2pActiveCode p2p = P2pActiveCode.findByMacAddr(pnid);
			p2pVo = new P2pActiveCodeVO(p2p);
			p2pVo.status = "New";
		}

		PageVO<PnRelationshipVO> pnidPageVO = new PageVO<PnRelationshipVO>(pnidPage);

		for(PnRelationship pn : pnidPage.getList()){
			PnRelationshipVO pnvo= new PnRelationshipVO(pn);

			if(pn.pn == null){
				pnvo.status = "Orphan";
			}else{
				pnvo.status = "Binding";
			}

            pnidPageVO.currents.add(pnvo);
		}

        return ok(admin_user_pnid.render(pnidPageVO, p2pVo, order, direction, pnid));
    }

    public Result listPnidAndDevId(String page, String userId){

        User user = User.find(userId);


        if(user == null){
            setErrorMessage("User Not Found!!");
            return redirect(routes.AdminUserController.manageUsers("0"));
        }

        DynamicForm df =formFactory.form().bindFromRequest();
        String order = df.get("order");
        String direction = df.get("direction");

        String orderByProp = orderByPropMap.get(order);

        if(orderByProp == null){
            orderByProp = "createAt";
        }

        Page<PnRelationship> pnidPage = PnRelationship.pnPageWithPnId(userId, orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);

        PageVO<PnRelationshipVO> pnidPageVO = new PageVO<PnRelationshipVO>(pnidPage);

        for(PnRelationship pn : pnidPage.getList()){
            PnRelationshipVO pnvo= new PnRelationshipVO(pn);

            pnidPageVO.currents.add(pnvo);
        }

        if(direction == null){
            direction = "desc";
        }

        if(order == null){
            order = "create";
        }

        P2pActiveCodeVO p2pVO = new P2pActiveCodeVO(null);

        return ok(admin_user_pnid.render(pnidPageVO, p2pVO, order, direction, null));
    }*/

    public Result getPnid(String userId){
        User user = User.find(userId);
        if(user == null){
            setErrorMessage("User Not Found!!");
            return redirect(routes.AdminUserController.manageUsers("0"));
        }
        List<MobileInfo> listMobileInfo = new ArrayList<>();
        MobileInfo mobileInfo;
        List<UserDevLog> userDevLog = UserDevLog.listUserDevLogByUserIdDistinctIMEI(userId);
        if(userDevLog == null){
            setErrorMessage("User Dev Log Not Found!!");
            return redirect(routes.AdminUserController.manageUsers("0"));
        }
        for(UserDevLog udl: userDevLog){
            mobileInfo = new MobileInfo();
            mobileInfo.appVer = udl.appVer;
            mobileInfo.deviceManufacturer = udl.deviceManufacturer;
            mobileInfo.deviceName = udl.deviceName;
            mobileInfo.deviceOsName = udl.deviceOsName;
            mobileInfo.deviceOsVersion = udl.deviceOsVersion;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss");
            mobileInfo.create_at = formatter.format(udl.createAt.getTime());
            listMobileInfo.add(mobileInfo);
        }
        Collections.sort(listMobileInfo, new comparelist());
        return ok(admin_user_pnidcheck.render(listMobileInfo));
    }

    static class comparelist implements Comparator<MobileInfo> {
        @Override
        public int compare(MobileInfo m1, MobileInfo m2){
            return m1.create_at.compareTo(m2.create_at);
        }
    }

    /*
    public Result getPnid(String page, String userId){
        User user = User.find(userId);

        if(user == null){
            setErrorMessage("User Not Found!!");
            return redirect(routes.AdminUserController.manageUsers("0"));
        }

            DynamicForm df =formFactory.form().bindFromRequest();
            String order = df.get("order");
            String direction = df.get("direction");
            String PnId = df.get("pnid");

            String orderByProp = orderByPropMap.get(order);

            if(orderByProp == null){
                orderByProp = "createAt";
            }
            Page<PnRelationship> pnidPage = PnRelationship.pnPageWithPnId(userId, orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
            PageVO<PnRelationshipVO> pnidPageVO = new PageVO<PnRelationshipVO>(pnidPage);

            for(PnRelationship p :pnidPage.getList()){
                PnRelationshipVO pnvo= new PnRelationshipVO(p);
                if(p.user == null){
                    pnvo.status = "Orphan";
                }else{
                    pnvo.status = "Binding";
                }

                pnidPageVO.currents.add(pnvo);
            }

            if(direction == null){
                direction = "desc";
            }

            if(order == null){
                order = "create";
            }

            P2pActiveCodeVO p2pVO = new P2pActiveCodeVO(null);
            return ok(admin_user_pnidcheck.render(pnidPageVO, p2pVO, order, direction, null));
    }
    */
}
