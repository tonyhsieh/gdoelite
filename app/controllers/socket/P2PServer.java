package controllers.socket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.Configuration;
import play.Logger;
import play.Logger.ALogger;
import play.mvc.Controller;
import utils.ExceptionUtil;
import utils.LoggingUtils;
import utils.SSLUtil;

import javax.net.ssl.SSLEngine;
import java.util.ArrayList;
import java.util.List;

public class P2PServer extends Controller implements Runnable {

    private Integer lstPort;

    private Configuration configuration;
    public P2PServer(Integer lstPort, Configuration configuration) {
        this.lstPort = lstPort;
        this.configuration = configuration;
    }

    @Override
    public void run() {
        String logHeader = "[P2PServer] run() - ";
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "Start P2PServer thread");

        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new P2PServerInitializer(configuration))
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            Channel channel = bootstrap.bind(lstPort).sync().channel();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "bootstrap.bind(" + lstPort + ")");
            channel.closeFuture().sync();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "ch.closeFuture().sync()");
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, logHeader + ExceptionUtils.getStackTrace(err));
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "workerGroup.shutdownGracefully()");
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "bossGroup.shutdownGracefully()");
        }
    }
}
