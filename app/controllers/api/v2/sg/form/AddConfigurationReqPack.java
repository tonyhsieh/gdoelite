package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class AddConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3590360091226791006L;

	@Constraints.Required
	public String xGenieMac;
	
	@Constraints.Required
	public String data;
	
	@Constraints.Required
	public String desc;
	
	@Constraints.Required
	public String cfgClass;
}
