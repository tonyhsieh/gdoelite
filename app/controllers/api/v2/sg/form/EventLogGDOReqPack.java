package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

/**
 * Created by joseph chou on 4/6/16.
 */
public class EventLogGDOReqPack extends BaseReqPack {

    @Constraints.Required
    public String xGenieMac;

    @Constraints.Required
    public String ts;

    @Constraints.Required
    public String door;

    @Constraints.Required
    public String event;

    @Constraints.Required
    public String user;
}
