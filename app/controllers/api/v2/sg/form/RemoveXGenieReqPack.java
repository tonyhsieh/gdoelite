package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class RemoveXGenieReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3126847284086581886L;

	@Constraints.Required
	public String xGenieInfo;
}
