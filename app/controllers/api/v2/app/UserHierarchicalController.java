package controllers.api.v2.app;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.*;
import controllers.api.v1.app.BaseAPIController;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v2.app.form.ListSmartGenieNetworkInfoReqpack;
import controllers.api.v1.app.form.*;
import frameworks.Constants;
import frameworks.models.IcDataId;
import frameworks.models.SGOwnership;
import frameworks.models.SmsCountryCodeType;
import frameworks.models.StatusCode;
import json.models.api.v1.app.*;
import json.models.api.v2.app.vo.SmartGenieInfoVO;
import json.models.api.v2.app.ListSmartGenieNetworkInfoRespPack;
import json.models.api.v1.app.vo.SmartGenieUserVO;
import json.models.api.v1.app.vo.UserSmartGenieVO;
import models.*;
import org.apache.commons.lang3.StringUtils;
import play.Configuration;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import services.SmartGenieService;
import services.UserRelationshipService;
import utils.ExceptionUtil;
import utils.LoggingUtils;
import utils.S3Manager;

import javax.inject.Inject;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/***
 *
 * @author johnwu
 *
 */
@PerformanceTracker
public class UserHierarchicalController extends BaseAPIController
{
    @Inject Configuration configuration;

    @SessionInject(AddNewUserReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result addNewUser()
    {
        AddNewUserReqPack pack = createInputPack();

        User owner = getUser();

        // get all smart genie of the user
        List<SmartGenie> listSmartGenie = SmartGenie.listSmartGenieByOwner(owner.id.toString());
        if (listSmartGenie.size() <= 0)
        {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");
        }

        // Find the joined user
        User user = User.findByEmail(pack.loginId);
        if (user == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "user not found");
        }

        // check the user already joined or not
        List<UserRelationship> listUserRelationship =
                UserRelationship.listUserRelationshipByOwnerNUser(
                        owner.id.toString(),
                        user.id.toString()
                );

        if (!listUserRelationship.isEmpty())
        {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_IS_JOINED, "user is joined");
        }

        // Add the new user to all SmartGenie of the owner
        for (SmartGenie smartGenie : listSmartGenie)
        {
            UserRelationship userRelationship = new UserRelationship();

            userRelationship.nickname = pack.nickname;
            userRelationship.email = pack.loginId;
            userRelationship.userId = user.id.toString();
            userRelationship.ownerId = owner.id.toString();
            userRelationship.smartGenieId = smartGenie.id;
            userRelationship.countryCode = getCountryCode(pack.countryCode);

            userRelationship.id = UserRelationship.genUserRelationshipId(
                    userRelationship.smartGenieId,
                    userRelationship.ownerId,
                    userRelationship.userId);

            userRelationship.save();
        }

        // Add user to user_relationship_keeper
        // If data is not exist, add user.
        UserRelationshipKeeper urk = UserRelationshipKeeper.FindByOwnerNUser(owner.id.toString(), user.id.toString());
        if (urk == null)
        {
            urk = new UserRelationshipKeeper();

            urk.nickname = pack.nickname;
            urk.email = pack.loginId;
            urk.userId = user.id.toString();
            urk.ownerId = owner.id.toString();
            urk.countryCode = getCountryCode(pack.countryCode);

            urk.save();
        }

        UserAccountRespPack respPack = new UserAccountRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.availableCountryCode = true;

        return createJsonResp(respPack);
    }

    @SessionInject(SessionReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result listSmartGenie()
    {
        User user = getUser();

        List<UserSmartGenieVO> usgVOList = new ArrayList<>();

        List<UserRelationship> userRelationshipList = UserRelationshipService.listUserRelationshipByUser(user.id.toString());

        for (UserRelationship item : userRelationshipList)
        {
            UserSmartGenieVO usgVO = new UserSmartGenieVO();
            SmartGenie sg = Ebean.find(SmartGenie.class, item.smartGenieId);
            usgVO.smartGenieId = sg.id;
            usgVO.smartGenieName = sg.name;
            usgVO.smartGenieStatusCode = sg.status;
            usgVO.userType = SGOwnership.USER.ordinal();
            usgVO.macAddr = sg.macAddress;
            SmartGenieLog sgLog = SmartGenieLog.find(sg.id);

            try
            {
                Calendar rightNow = Calendar.getInstance();
                if (sgLog.logDateTime != null && (rightNow.getTimeInMillis() - sgLog.logDateTime.getTimeInMillis()) < 2 * Constants.PING_TIMER)
                {
                    usgVO.isALive = 1;
                }
                else
                {
                    usgVO.isALive = 0;
                }
            }
            catch (Exception err)
            {
                usgVO.isALive = 0;
            }

            usgVO.lat = sgLog.lat;
            usgVO.lng = sgLog.lng;

            usgVOList.add(usgVO);
        }

        List<SmartGenie> smartGenieList = SmartGenieService.listSmartGenieByOwner(user.id.toString());
        for (SmartGenie item : smartGenieList)
        {
            UserSmartGenieVO usgVO = new UserSmartGenieVO();
            usgVO.smartGenieId = item.id;
            usgVO.smartGenieName = item.name;
            usgVO.smartGenieStatusCode = item.status;
            usgVO.userType = SGOwnership.OWNER.ordinal();
            usgVO.macAddr = item.macAddress;

            //add by Jack Chuang 2013 06 27
            SmartGenieLog sgLog = SmartGenieLog.find(item.id);

            usgVO.lat = sgLog.lat;
            usgVO.lng = sgLog.lng;

            try
            {
                Calendar rightNow = Calendar.getInstance();
                if (sgLog.logDateTime != null && (rightNow.getTimeInMillis() - sgLog.logDateTime.getTimeInMillis()) < 2 * Constants.PING_TIMER)
                {
                    usgVO.isALive = 1;
                }
                else
                {
                    usgVO.isALive = 0;
                }
            }
            catch (Exception err)
            {
                usgVO.isALive = 0;
            }

            usgVOList.add(usgVO);
        }

        SmartGenieRespPack respPack = new SmartGenieRespPack();
        respPack.sessionId = user.id.toString();
        respPack.listSmartGenie = usgVOList;

        return createJsonResp(respPack);
    }

    @SessionInject(ListSmartGenieNetworkInfoReqpack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result SmartGenieNetworkInfo() {
        String SG_photo;
        if (configuration.getString("application.environment").equals("PROD"))
        {
            SG_photo = S3Manager.ProdS3Bucket.SG_PHOTO.value;
        }
        else if (configuration.getString("application.environment").equals("DEV"))
        {
            SG_photo = S3Manager.DevS3Bucket.SG_PHOTO.value;
        }
        else
        {
            SG_photo = S3Manager.StageS3Bucket.SG_PHOTO.value;
        }
        ListSmartGenieNetworkInfoReqpack pack = createInputPack();
        User user = getUser();
        boolean isAtLan;
        Http.RequestHeader httpservletrequest = request();
        if (httpservletrequest == null)
            return null;
        //Get Mobile external IP address
        String mobileExtIP = httpservletrequest.getHeader("X-Forwarded-For");
        if (mobileExtIP == null || mobileExtIP.length() == 0 || "unknown".equalsIgnoreCase(mobileExtIP))
            mobileExtIP = httpservletrequest.getHeader("Proxy-Client-IP");
        if (mobileExtIP == null || mobileExtIP.length() == 0 || "unknown".equalsIgnoreCase(mobileExtIP))
            mobileExtIP = httpservletrequest.getHeader("WL-Proxy-Client-IP");
        if (mobileExtIP == null || mobileExtIP.length() == 0 || "unknown".equalsIgnoreCase(mobileExtIP))
            mobileExtIP = httpservletrequest.getHeader("HTTP_CLIENT_IP");
        if (mobileExtIP == null || mobileExtIP.length() == 0 || "unknown".equalsIgnoreCase(mobileExtIP))
            mobileExtIP = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");
        if (mobileExtIP == null || mobileExtIP.length() == 0 || "unknown".equalsIgnoreCase(mobileExtIP))
            mobileExtIP = httpservletrequest.remoteAddress();
        if ("127.0.0.1".equals(mobileExtIP) || "0:0:0:0:0:0:0:1".equals(mobileExtIP))
        {
            try
            {
                mobileExtIP = InetAddress.getLocalHost().getHostAddress();
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
            }
        }
        SmartGenieInfoVO usgVO = new SmartGenieInfoVO();
        SmartGenie sg = SmartGenie.findByMacAddress(pack.smartGenieMac);
        if(sg == null){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");
        }
        usgVO.smartGenieId = sg.id;
        usgVO.smartGenieName = sg.name;
        usgVO.smartGenieStatusCode = sg.status;
        if(sg.owner.id.toString().equals(user.id.toString())){
            usgVO.userType = SGOwnership.OWNER.ordinal();
        }else{
            usgVO.userType = SGOwnership.USER.ordinal();
        }
        usgVO.devType = sg.devType;
        usgVO.photoURL = "";
        if (sg.photoFileId != null) {
            FileEntity file = Ebean.find(FileEntity.class, sg.photoFileId.id.toString());
                if (file != null) {
                    usgVO.photoURL = configuration.getString("s3.buckets.url") + SG_photo + "/" + file.s3Path;
                }
            }
            if (sg.devType == null) {
                usgVO.devType = 0;
            }
            usgVO.macAddr = sg.macAddress;
            if (sg.owner == null || sg.owner.id == null) {
                usgVO.ownerId = "0";
            } else {
                usgVO.ownerId = sg.owner.id.toString();
            }
            //Get the AG alive or not
            SmartGenieLog sgLog = SmartGenieLog.find(sg.id);
            if(sgLog == null){
                return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "smart genie log is null");
            }
            try {
                Calendar rightNow = Calendar.getInstance();
                if (sgLog.logDateTime != null && (rightNow.getTimeInMillis() - sgLog.logDateTime.getTimeInMillis()) < 2 * Constants.PING_TIMER) {
                    usgVO.isALive = 1;
                }
                else {
                    usgVO.isALive = 0;
                }
            }
            catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "update smart genie log is Alive error" + ExceptionUtil.exceptionStacktraceToString(err));
                usgVO.isALive = 0;
            }

            //compare Smart Genie and Mobile external IP address
            isAtLan = mobileExtIP.equals(sgLog.extIp);

            //compare mobile and Smart Genie networking information
            if (isAtLan) {
                Pattern pat;
                pat = Pattern.compile("[.]");
                String[] strSgIP = pat.split(sgLog.innIp);
                String[] strSgNetmask = pat.split(sgLog.netMask);
                String[] strMobileIP = pat.split(pack.innIp);
                String[] strMobileNetmask = pat.split(pack.netMask);
                int[] iSgIP = new int[strSgIP.length];
                int[] iSgNetmask = new int[strSgNetmask.length];
                int[] iMobileIP = new int[strMobileIP.length];
                int[] iMobileNetmask = new int[strMobileNetmask.length];
                for (int i = 0; i < strSgIP.length; i++) {
                    iSgIP[i] = Integer.parseInt(strSgIP[i]);
                    iSgNetmask[i] = Integer.parseInt(strSgNetmask[i]);
                    iMobileIP[i] = Integer.parseInt(strMobileIP[i]);
                    iMobileNetmask[i] = Integer.parseInt(strMobileNetmask[i]);
                }
                for (int i = 0; i < strSgIP.length; i++) {
                    if ((iSgIP[i] & iSgNetmask[i]) != (iMobileIP[i] & iMobileNetmask[i]))
                    {
                        isAtLan = false;
                        break;
                    }
                }
            }
            usgVO.isAtLan = isAtLan;
            usgVO.smartGenieInnerIP = sgLog.innIp;
            usgVO.smartGenieNetmask = sgLog.netMask;
            usgVO.wifiMacAddress = sgLog.wifiMacAddress;
            usgVO.fwVer = sgLog.firmwareVer;
            usgVO.lat = sgLog.lat;
            usgVO.lng = sgLog.lng;




        ListSmartGenieNetworkInfoRespPack respPack = new ListSmartGenieNetworkInfoRespPack();
        respPack.sessionId = user.id.toString();
        respPack.smartGenieInfo = usgVO;

        return createJsonResp(respPack);
    }

    @SessionInject(SessionReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result listUserInfo()
    {
        SessionReqPack pack = createInputPack();

        User owner = getUser();

        List<SmartGenieUserVO> sgUserVOList = new ArrayList<>();

        List<UserRelationshipKeeper> listUrk = UserRelationshipKeeper.listByOwner(owner.id.toString());
        // users' info for current owner
        for (UserRelationshipKeeper urk : listUrk)
        {
            SmartGenieUserVO sguVO = new SmartGenieUserVO();

            try
            {
                sguVO.userType = SGOwnership.USER.ordinal();
                sguVO.userId = urk.userId;
                sguVO.nickName = urk.nickname;

                User user = User.find(urk.userId);
                if (user == null)
                {
                    continue;
                }

                sguVO.email = user.email;
                if (user.gateway != null)
                {
                    sguVO.gatewayId = user.gateway.id;
                    sguVO.gatewayName = user.gateway.carrier;
                }
                sguVO.countryCode = user.countryCode;
                sguVO.cellPhone = user.cellPhone;

                sgUserVOList.add(sguVO);
            }
            catch (Exception err)
            {
                err.printStackTrace();
            }
        }

        // current owner's info
        SmartGenieUserVO sguVOOwner = new SmartGenieUserVO();
        sguVOOwner.userId = owner.id.toString();
        sguVOOwner.nickName = owner.nickname;
        sguVOOwner.countryCode = owner.countryCode;
        sguVOOwner.cellPhone = owner.cellPhone;
        sguVOOwner.userType = SGOwnership.OWNER.ordinal();
        sguVOOwner.email = owner.email;
        if (owner.gateway != null)
        {
            sguVOOwner.gatewayId = owner.gateway.id;
            sguVOOwner.gatewayName = owner.gateway.carrier;
        }
        sgUserVOList.add(sguVOOwner);
        Collections.sort(sgUserVOList, new comparelist());
        UserInfoBySGRespPack respPack = new UserInfoBySGRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.listUser = sgUserVOList;
        respPack.listGateway = Email2TextController.getGatewayVOs();

        return createJsonResp(respPack);
    }

    static class comparelist implements Comparator<SmartGenieUserVO>{
        @Override
        public int compare(SmartGenieUserVO s1, SmartGenieUserVO s2){
             if(s1.userType != s2.userType){
                return Integer.compare(s1.userType, s2.userType);
            }else{
               return s1.nickName.compareTo(s2.nickName);
            }
        }
    }



    @SessionInject(GetOwnerInfoReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result getOwnerInfoBySgMac(String sgMac)
    {
        GetOwnerInfoReqPack pack = createInputPack();

        User user = getUser();

        SmartGenie sg = SmartGenie.findByMacAddress(sgMac);

        if (sg == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");
        }

        List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipBySG(sg.id);

        UserRelationship userRelationship = null;

        boolean isSgUser = false;

        for (int i = 0; i < listUserRelationship.size(); i++)
        {
            userRelationship = listUserRelationship.get(i);
            try
            {
                if (user.id.toString().equals(userRelationship.userId))
                {
                    isSgUser = true;
                }
            }
            catch (Exception err)
            {
                err.printStackTrace();
            }
        }

        try
        {
            if (user.id.toString().equals(sg.owner.id.toString()))
            {
                isSgUser = true;
            }
        }
        catch (Exception err)
        {
            err.printStackTrace();
        }

        if (!isSgUser)
        {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_MEMBERSHIP_ERROR, "smartgenie membership error");
        }

        OwnerInfoBySgMacRespPack respPack = new OwnerInfoBySgMacRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.email = sg.owner.email;
        respPack.nickname = sg.owner.nickname;
        respPack.userId = sg.owner.id.toString();

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(AddNewUserReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result editUser()
    {
        AddNewUserReqPack pack = createInputPack();

        User owner = getUser();

        final boolean isUsingEmail2Text = isUsingEmail2Text(pack);
        final Gateway gateway = getGatewayFromId(pack.gatewayId);
        if (isUsingEmail2Text && gateway == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.WRONG_PARAMETER, "email2text gateway not found");
        }

        if (owner.email.equals(pack.loginId))
        {
            owner.nickname = pack.nickname;

            owner.countryCode = getCountryCode(pack.countryCode);
            owner.cellPhone = pack.cellPhone;
            if (isUsingEmail2Text)
            {
                owner.gateway = gateway;
            }
            owner.save();
        }
        else
        {
            //Find the joined user
            User user = User.findByEmail(pack.loginId);

            if (user == null)
            {
                return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "user not found");
            }

            List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipByOwnerNUser(
                    owner.id.toString(),
                    user.id.toString()
            );

            for (UserRelationship userRelationship : listUserRelationship)
            {
                userRelationship.nickname = pack.nickname;
                userRelationship.save();
            }

            // Add user to user_relationship_keeper
            UserRelationshipKeeper urk = UserRelationshipKeeper.FindByOwnerNUser(owner.id.toString(), user.id.toString());
            if (urk != null)
            {
                urk.nickname = pack.nickname;
                urk.save();
            }
        }

        UserAccountRespPack respPack = new UserAccountRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.availableCountryCode = true;

        return createJsonResp(respPack);
    }
    @BodyParser.Of(BodyParser.AnyContent.class)
    @SessionInject(AddNewUserReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result removeUser()
    {
        AddNewUserReqPack pack = createInputPack();

        User owner = getUser();

        ObjectMapper jsonMapper = new ObjectMapper();

        ListRemoveUserEmailReqPack listUser = null;

        try
        {
            listUser = jsonMapper.readValue(pack.loginId, ListRemoveUserEmailReqPack.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        for (int i = 0; i < listUser.AccountEmail.size(); i++)
        {
            try
            {
                //Find the user
                User user = User.findByEmail(listUser.AccountEmail.get(i).email);

                if (user != null)
                {
                    List<UserRelationship> listUserRelationship =
                            UserRelationship.listUserRelationshipByOwnerNUser(
                                    owner.id.toString(),
                                    user.id.toString()
                            );

                    for (int j = 0; j < listUserRelationship.size(); j++)
                    {
                        UserRelationshipHistoryLog log = new UserRelationshipHistoryLog();
                        log.ownerId = listUserRelationship.get(j).ownerId;
                        log.userId = listUserRelationship.get(j).userId;
                        log.smartGenieId = listUserRelationship.get(j).smartGenieId;
                        log.save();

                        // 2014 01 21 Jack Chuang
                        // List XG by SmartGenie
                        SmartGenie sg;

                        sg = SmartGenie.find(listUserRelationship.get(j).smartGenieId);

                        // List EventContact by XG
                        List<XGenie> listXg;

                        listXg = XGenie.listXGBySG(sg);


                        for (XGenie xg : listXg)
                        {
                            // List Contact by EventContactId & user
                            List<EventContact> listEc;

                            listEc = EventContact.listByMac(xg.macAddr);

                            List<Contact> listContact;

                            for (EventContact ec : listEc)
                            {
                                listContact = Contact.listByEventContactNUserId(ec.id, user.id.toString());

                                // remove contact data of the user
                                for (Contact contact : listContact)
                                {
                                    contact.delete();
                                }
                            }
                        }
                        listUserRelationship.get(j).delete();
                    }

                    // Add user to user_relationship_keeper
                    UserRelationshipKeeper urk = UserRelationshipKeeper.FindByOwnerNUser(owner.id.toString(), user.id.toString());
                    if (urk != null)
                    {
                        urk.delete();
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        SessionRespPack respPack = new SessionRespPack();
        respPack.sessionId = pack.sessionId;

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(GetWateringHistoryReqPack.class)
    @ParameterInject(GetWateringHistoryReqPack.class)
    public Result getIcWateringHistory(String xgMAC)
    {
        StatusCode statusCode = StatusCode.GENERAL_SUCCESS;

        GetWateringHistoryReqPack pack = createInputPack();

        IcEventStatsAgent eventStatsAgent = new IcEventStatsAgent();
        AbstractIcEventStats strategy = null;
        IcWateringTime[] log;

        try
        {
            byte zoneId = pack.getZoneId();
            byte dataId = pack.getDataId();
            short offset = pack.getOffset();

            XGenie xGenie = XGenie.findByXgMac(xgMAC);
            if (xGenie == null)
            {
                statusCode = StatusCode.XGENIE_NOTFOUND;
                throw new Exception("XGenie not found with MAC address " + xgMAC);
            }

            IcDataId icDataId = IcDataId.fromValue(dataId);

            switch (icDataId)
            {
                case DAILY: // 1 day
                    strategy = new IcEventStatsDaily();
                    break;
                case WEEKLY: // 1 week
                    strategy = new IcEventStatsWeekly();
                    break;
                case MONTHLY: // 1 month
                    strategy = new IcEventStatsMonthly();
                    break;
                case YEARLY: // 1 year
                    strategy = new IcEventStatsYearly();
                    break;
                default:
                    break;
            }

            log = eventStatsAgent.getIcWateringTime(strategy, xGenie, zoneId, offset);
        }
        catch (NumberFormatException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            return createIndicatedJsonStatusResp(statusCode, e.getMessage());
        }

        DateFormat sdf = new SimpleDateFormat(Constants.JSON_CALENDAR_PATTERN);
        GetIcWateringRespPack respPack = new GetIcWateringRespPack(log);
        respPack.setStartTime(sdf.format(strategy.getStartTime()));
        respPack.setEndTime(sdf.format(strategy.getEndTime()));
        respPack.setSessionId(pack.sessionId);

        return createJsonResp(respPack);
    }

    private static boolean isUsingEmail2Text(final AddNewUserReqPack pack)
    {
        return (StringUtils.isNotEmpty(pack.cellPhone)
                && StringUtils.isNotEmpty(pack.gatewayId)
                && !pack.gatewayId.equals("0"));
    }

    private static Gateway getGatewayFromId(final String gatewayId)
    {
        try
        {
            int gid = Integer.parseInt(gatewayId);
            return Gateway.getById(gid);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private static String getCountryCode(final String countryCode)
    {
        if (StringUtils.isEmpty(countryCode))
        {
            return SmsCountryCodeType.US;
        }
        return countryCode;
    }
}
