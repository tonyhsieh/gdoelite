package controllers.api.v1.sg.cache;

import java.io.Serializable;

public class SGSessionCache implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2835461666723644301L;

	public String sgId;
	
	@Override
	public String toString() {
		return "Session ["
			+ (sgId != null ? "sgId="	+ sgId + ", " : "")
			+ "]";
	}
}
