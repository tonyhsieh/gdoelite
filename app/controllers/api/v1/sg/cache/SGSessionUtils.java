package controllers.api.v1.sg.cache;

import com.avaje.ebean.Ebean;

import java.util.ArrayList;
import java.util.List;

import models.SmartGenie;
import utils.CacheManager;
import frameworks.models.CacheKey;

/**
 * @author johnwu
 * @version 創建時間：2013/8/12 上午11:31:24
 */
public class SGSessionUtils {
	
	public static synchronized boolean login(String sessionId, String sgId){
		SGSessionCache usc = new SGSessionCache();
		usc.sgId = sgId;
		CacheManager.setWithId(CacheKey.SG_SESSION, sessionId, usc, CacheManager.SGSESSION_EXPIRED_TIME);
		
		SGSessionList sessionList = CacheManager.get(CacheKey.SG_SESSION_LIST);
		
		if(sessionList == null){
			sessionList = new SGSessionList();
		}
		
		List<SGSessionData> currentSessionList = sessionList.currentSessionList;
		
		if(currentSessionList == null){
			currentSessionList = new ArrayList<SGSessionData>();
		}
		
		SGSessionData sd = new SGSessionData(sessionId, sgId);
		currentSessionList.add(sd);
		CacheManager.set(CacheKey.SG_SESSION_LIST, sessionList);
		
		return (CacheManager.get(CacheKey.SG_SESSION, sessionId) != null);
	}
	
	public static synchronized boolean logout(String sessionId){
		CacheManager.removeWithId(CacheKey.SG_SESSION, sessionId);
		
		SGSessionList sessionList = CacheManager.get(CacheKey.SG_SESSION_LIST);
		
		if(sessionList != null){
			if(sessionList.currentSessionList != null){
				List<SGSessionData> sgSessionList = sessionList.currentSessionList;
				for(SGSessionData sessionData:sgSessionList){
					if(sessionData.sessionId.equals(sessionId)){
						sessionList.currentSessionList.remove(sessionData);
						CacheManager.set(CacheKey.SG_SESSION_LIST, sessionList);
						break;
					}
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
		
		return (CacheManager.get(CacheKey.SG_SESSION, sessionId) == null);
	}

	public static SmartGenie getSmartGenie(String sessionId){
		SGSessionCache ssc = CacheManager.get(CacheKey.SG_SESSION, sessionId);

		if(ssc == null){
			return null;
		}

		return Ebean.find(SmartGenie.class, ssc.sgId);
	}
}
