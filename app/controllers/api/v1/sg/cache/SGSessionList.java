package controllers.api.v1.sg.cache;

import java.io.Serializable;
import java.util.List;

public class SGSessionList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5726724555955199589L;

	public List<SGSessionData> currentSessionList;
}
