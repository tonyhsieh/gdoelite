package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class SwitchConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2874960518241258353L;

	@Constraints.Required
	public String data;
}
