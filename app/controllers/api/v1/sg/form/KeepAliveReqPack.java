package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class KeepAliveReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4249022235776794503L;

	@Constraints.Required
	public String sessionId;
}
