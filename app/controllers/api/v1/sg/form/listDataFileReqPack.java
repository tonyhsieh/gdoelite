package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class listDataFileReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2673148496142229288L;

	@Constraints.Required
	public String sgMac;
	
	@Constraints.Required
	public String fileType;
}
