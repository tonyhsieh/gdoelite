package controllers.api.v1.sg.form;

import java.util.List;

import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.ListRemoveUserEmailReqPack.ListEmail;

public class ListRemoveXGenieReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5621650813692098856L;

	public List<XGenie> xGenie;
	
	public static class XGenie{
		public String xgId;
	}
}
