package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class AddConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3590360091226791006L;

	@Constraints.Required
	public String xGenieId;
	
	@Constraints.Required
	public String data;
	
	@Constraints.Required
	public String desc;
	
	@Constraints.Required
	public String cfgClass;
}
