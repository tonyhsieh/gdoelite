package controllers.api.v1.sg;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.IcEventLogReqPack;
import frameworks.models.IcDataId;
import frameworks.models.StatusCode;
import json.models.api.v1.sg.SwitchConfigurationRespPack;
import models.IcEventLog;
import models.IcEventLogId;
import models.IcEventStats;
import models.XGenie;
import play.mvc.Result;

public class IrrigationController extends BaseAPIController {

	private static StatusCode statusCode = StatusCode.GENERAL_SUCCESS;
	private static final byte MINUTES_IN_HOUR = (byte) TimeUnit.HOURS.toMinutes(1);
	
	@Transactional
    @SessionInject(IcEventLogReqPack.class)
	public Result postIcEventLog() {
		statusCode = StatusCode.GENERAL_SUCCESS;
		
		IcEventLogReqPack reqPack = createInputPack();
		
		try {
			String macAddr = reqPack.getMacAddr();
			XGenie xGenie = XGenie.findByXgMac(macAddr);
			if (xGenie == null) {
				statusCode = StatusCode.XGENIE_NOTFOUND;
				throw new Exception("XGenie not found with MAC address " + macAddr);
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
			Date startTime = sdf.parse(reqPack.getStartTime());
			
			IcEventLogId icEventLogId = new IcEventLogId(xGenie.id, reqPack.getZoneId(), reqPack.getEventType(), startTime);
			IcEventLog icEventLog = new IcEventLog(icEventLogId, reqPack.getDuration());
			
			IrrigationController ic = new IrrigationController();
			if (icEventLog.hasOverlappeRecord()) {
				statusCode = StatusCode.CAN_NOT_WRITE_TO_DATABASE;
				throw new Exception("Log record time is overlapped with existing item");
			}
			
			Ebean.beginTransaction();
			try {
				icEventLog.save();
			
				ic.insertHourlyStats(icEventLog);
				
				Ebean.commitTransaction();
			} catch (Exception e) {
				Ebean.rollbackTransaction();
				
				statusCode = StatusCode.CAN_NOT_WRITE_TO_DATABASE;
				throw new Exception("Error saving IcEventLog into DB: " + e.getMessage());
			} finally {
			    Ebean.endTransaction();
			}
		} catch (Exception e) {
			return createIndicatedJsonStatusResp(statusCode, e.getMessage());
		}
		
		SwitchConfigurationRespPack respPack = new SwitchConfigurationRespPack();
		respPack.sessionId = reqPack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	
	private void insertHourlyStats(IcEventLog icEventLog) {
		Calendar startTime = Calendar.getInstance();
		IcEventLogId icEventLogId = icEventLog.getIcEventLogId();
		startTime.setTime(icEventLogId.getStartTime());
		int min = startTime.get(Calendar.MINUTE);
		int duration = icEventLog.getDuration();
		startTime.set(Calendar.MINUTE, 0);
		byte zoneId = icEventLogId.getZoneId();
		
		int mins = min + duration;
		while (mins > 0) {
			int minToSave = 0;
			if (mins > MINUTES_IN_HOUR) {
				mins = mins - MINUTES_IN_HOUR;
				minToSave = MINUTES_IN_HOUR - min;
				duration -= minToSave;
				min = 0;
			} else {
				minToSave = duration;
				mins = 0;
			}
			
			IcEventStats stats = IcEventStats.find(IcDataId.toValue(IcDataId.HOURLY), icEventLogId.getxGenieId(),
					zoneId, icEventLogId.getEventType(), startTime.getTime());
			if (stats != null) {
				stats.setCumulativeMinutes(stats.getCumulativeMinutes() + minToSave);
				stats.update();
			} else {
				stats = new IcEventStats(IcDataId.toValue(IcDataId.HOURLY), icEventLogId.getxGenieId(), zoneId,
						icEventLogId.getEventType(), startTime.getTime(), (byte) (minToSave));
				stats.save();
			}
					
			startTime.set(Calendar.HOUR, startTime.get(Calendar.HOUR) + 1);
		}
	}
	
}
