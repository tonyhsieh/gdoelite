package controllers.api.v1.sg;

import models.P2pActiveCode;
import json.models.api.v1.sg.ChkAgLegalRespPack;
import play.mvc.Result;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.ParameterInject;
import controllers.api.v1.sg.form.ChkAgLegalReqPack;

@PerformanceTracker
public class P2pController extends BaseAPIController{

	@Transactional
	@ParameterInject(ChkAgLegalReqPack.class)
	public Result chkAgLegal(String mac){
		
		ChkAgLegalReqPack pack = createInputPack();

		P2pActiveCode activeCode = P2pActiveCode.find(mac);
		
		ChkAgLegalRespPack respPack = new ChkAgLegalRespPack();
		
		if(pack.actionCode==null){
			respPack.legal = false;
		}else{
			if(activeCode==null){
				respPack.legal = false;
			}else{
				if(pack.actionCode.equals(activeCode.active_code)){
					respPack.legal = true;
				}else{
					respPack.legal = false;
				}
			}
		}
		
		return createJsonResp(respPack);
	}
}
