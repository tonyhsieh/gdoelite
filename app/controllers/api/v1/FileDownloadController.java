/**
 *
 */
package controllers.api.v1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import models.FileEntity;

import org.springframework.stereotype.Controller;

import play.mvc.Result;
import utils.db.FileEntityManager;

import com.avaje.ebean.Ebean;

import controllers.BaseController;
import frameworks.models.FileType;

/**
 * @author carloxwang
 *
 */
@Controller
public class FileDownloadController extends BaseController {

	private final static Map<String, FileType> fileTypeMap = new HashMap<String, FileType>();

	static{
		fileTypeMap.put("sgconf", FileType.SG_CONFIGURATION);
		fileTypeMap.put("sgfw", FileType.SG_FIRMWARE);
		fileTypeMap.put("xgfw", FileType.XG_FIRMWARE);
	}

	public Result download(String fileId, String fileType){

		try{
			FileEntity fn = Ebean.find(FileEntity.class, fileId);

			if ( fn == null ) throw new IllegalArgumentException();

			return ok(FileEntityManager.getFile(fn, fileTypeMap.get(fileType)));

		} catch (IllegalArgumentException e) {

			return notFound();
		} catch (IOException e) {

			return internalServerError(e.getMessage());
		}
	}
}
