package controllers.api.v1.app;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.models.api.v1.app.UserEventContactRespPack;
import json.models.api.v1.app.UserRespPack;
import json.models.api.v1.app.vo.EventContactData;
import json.models.api.v1.app.vo.EventContactVO;
import models.Contact;
import models.EventContact;
import models.SmartGenie;
import models.User;
import models.UserRelationship;
import models.XGenie;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import play.mvc.Result;
import utils.LoggingUtils;
import utils.generator.CredentialGenerator;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.EventContactReqPack;
import controllers.api.v1.app.form.UserEventContactReqPack;
import frameworks.models.StatusCode;

@PerformanceTracker
public class ContactController extends BaseAPIController{

	@Transactional
	@MobileLogInject(EventContactReqPack.class)
	@SessionInject(EventContactReqPack.class)
	public Result addEventContact(String macAddr){

		EventContactReqPack pack = createInputPack();

		User appUser = getUser();
		
		User owner = null;
		User user = null;
		
		/*
		 * For add AG Event Contact
		 * By Jack 2015 09 04
		 */
		//Is AG existed ?
		SmartGenie sg = null;
		
		XGenie xGenie = null;
		
		String macAddress = "";
		
		try{
			sg = SmartGenie.findByMacAddress(macAddr);
		}catch(Exception e){
			
		}
		/////////////////////////////////////////////////////////
		
		//xGenied is existed?
		try{
			xGenie = XGenie.findByXgMac(macAddr);
		}catch(Exception e){
			
		}
		
		if(xGenie != null){
			
			macAddress = xGenie.macAddr;
			sg = xGenie.smartGenie;
			
			//xGenie's owner check
			if(!sg.owner.id.toString().equals(appUser.id.toString())){
				return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "ownership error");
			}
			
		}else if(sg != null){
			macAddress = sg.macAddress;
		}

		if(xGenie == null && sg == null){
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "not found error");
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////

		//the invitees is owner or user?
		if(StringUtils.isNotBlank(pack.userId)){	//is User
			
			UserRelationship urs = null;
			
			try{
				urs = 
					Ebean.find(
							UserRelationship.class, 
							UserRelationship.genUserRelationshipId(
									sg.id, 
									appUser.id.toString(), 
									pack.userId));
			}catch(Exception err){
				return createIndicatedJsonStatusResp(StatusCode.USER_RELATIONSHIP_ERROR, "relationship error");
			}
			
			user = appUser;

			if(urs == null){
				return createIndicatedJsonStatusResp(StatusCode.USER_RELATIONSHIP_ERROR, "relationship error");
			}
			
		}else{									//is Owner
			owner = appUser;
			pack.userId = appUser.id.toString();
		}

		//request data integrated
		EventContactVO ecDataVO = null;
		ObjectMapper om = new ObjectMapper();
		try {
			ecDataVO = om.readValue(pack.params, EventContactVO.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "unknown error");
		}
        User u = User.find(pack.userId);
		if(u.cellPhone == null){
            return createIndicatedJsonStatusResp(StatusCode.USER_WITH_NO_PHONE_NUMBER, "user have no cellphone");
        }

		//access all contact method
		for(EventContactData ecData:ecDataVO.ecDataList){
			EventContact ec = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, ecData.eventType));
			
			//if ecType = 10100(Door 1 Opened) or 10101(Door 1 Closed)
			//Add ecType 10102(Door 2 Opened) or 10103(Door 2 Closed) Automatic
			//by Jack Chuang 2014 02 07
			EventContact ecTmp = null;
			if(ecData.eventType==10100){
				ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, 10102));
			}
			
			if(ecData.eventType==10101){
				ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, 10103));
			}
			
			//if ecType = -10101(Door 1 Sensor Battery Low) or -10102(Door 1 Sensor not responding)
			//Add ecType -10103(Door 2 Sensor Battery Low) or -10104(Door 2 Sensor not responding) Automatic
			//by Jack Chuang 2014 02 07
			if(ecData.eventType==-10101){
				ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, -10103));
			}
			
			if(ecData.eventType==-10102){
				ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, -10104));
			}
			//end
			
			if(ec == null){
				ec = new EventContact();
				ec.id = CredentialGenerator.genEventContactId(macAddress, ecData.eventType);
				ec.eventType = ecData.eventType;
				ec.deviceMac = macAddress;
				ec.save();
			}
			
			
			//if ecType = 10100(Door 1 Opened) or 10101(Door 1 Closed)
			//Add ecType 10102(Door 2 Opened) or 10103(Door 2 Closed) Automatic
			//by Jack Chuang 2014 02 07
			if(ecTmp==null){
				
				if(ecData.eventType==10100){
					ecTmp = new EventContact();
					ecTmp.id = CredentialGenerator.genEventContactId(macAddress, 10102);
					ecTmp.eventType = 10102;
					ecTmp.deviceMac = macAddress;
					ecTmp.save();
				}
				
				if(ecData.eventType==10101){
					ecTmp = new EventContact();
					ecTmp.id = CredentialGenerator.genEventContactId(macAddress, 10103);
					ecTmp.eventType = 10103;
					ecTmp.deviceMac = macAddress;
					ecTmp.save();
				}
				
				if(ecData.eventType==-10101){
					ecTmp = new EventContact();
					ecTmp.id = CredentialGenerator.genEventContactId(macAddress, -10103);
					ecTmp.eventType = -10103;
					ecTmp.deviceMac = macAddress;
					ecTmp.save();
				}
				
				if(ecData.eventType==-10102){
					ecTmp = new EventContact();
					ecTmp.id = CredentialGenerator.genEventContactId(macAddress, -10104);
					ecTmp.eventType = -10104;
					ecTmp.deviceMac = macAddress;
					ecTmp.save();
				}
			}
			//end
			
			////////////////////////////////////////////////////////////////////////////////
			Contact contact = Ebean.find(Contact.class, CredentialGenerator.genContactId(ec.id, pack.userId));
			
			if(contact == null){
				contact = new Contact();
				contact.id = CredentialGenerator.genContactId(ec.id, pack.userId);
				contact.userId = pack.userId;
				contact.eventContact = ec;
			}
			contact.notifyEmail = ecData.checkEmail;
			contact.notifyPushNotification = ecData.checkNotification;
            contact.notifySMS = ecData.checkSMS;

			/*
			 * If passive device event type != 10002, 10003, -10150, -10151
			 * contact.notifySMS MUST = false
			 * by Jack 2015 09 04
			*/
			if(ecData.eventType!=10002 && ecData.eventType!=10003 && 
				ecData.eventType!=10100 && ecData.eventType!=10101 && 
				ecData.eventType!=10102 && ecData.eventType!=10103 && 
				ecData.eventType!=-10150 && ecData.eventType!=-10151){
				contact.notifySMS = false;
			}

			contact.save();
			/////////////////////////////////////////////////////////////////////
			
			//if ecType = 10100(Door 1 Opened) or 10101(Door 1 Closed)
			//Add ecType 10102(Door 2 Opened) or 10103(Door 2 Closed) Automatic
			
			//if ecType = -10101(Door 1 Sensor Battery Low) or -10102(Door 1 Sensor not responding)
            //Add ecType -10103(Door 2 Sensor Battery Low) or -10104(Door 2 Sensor not responding) Automatic
			
			//by Jack Chuang 2014 02 07
			Contact contactTmp = null;
			
            if(ecData.eventType==10100 || ecData.eventType==10101 ||
            		ecData.eventType==-10101 || ecData.eventType==-10102){
            	
                contactTmp = Ebean.find(Contact.class, CredentialGenerator.genContactId(ecTmp.id, pack.userId));
                
                if(contactTmp == null){
                	contactTmp = new Contact();
					contactTmp.id = CredentialGenerator.genContactId(ecTmp.id, pack.userId);
					contactTmp.userId = pack.userId;
					contactTmp.eventContact = ecTmp;
                }
                
                contactTmp.notifyEmail = ecData.checkEmail;
				contactTmp.notifyPushNotification = ecData.checkNotification;
                contactTmp.notifySMS = ecData.checkSMS;

				contactTmp.save();
            }

			/*
            //if ecType = -10101(Door 1 Sensor Battery Low) or -10102(Door 1 Sensor not responding)
            //Add ecType -10103(Door 2 Sensor Battery Low) or -10104(Door 2 Sensor not responding) Automatic
            if(ecData.eventType==-10101 || ecData.eventType==-10102){
                contactTmp = Ebean.find(Contact.class, CredentialGenerator.genContactId(ecTmp.id, pack.userId));
            }

			//if ecType = 10100(Door 1 Opened) or 10101(Door 1 Closed)
			//Add ecType 10102(Door 2 Opened) or 10103(Door 2 Closed) Automatic
			//by Jack Chuang 2014 02 07
			if(contactTmp == null){
				if(ecData.eventType==10100 || ecData.eventType==10101){
					contactTmp = new Contact();
					contactTmp.id = CredentialGenerator.genContactId(ecTmp.id, pack.userId);
					contactTmp.userId = pack.userId;
					contactTmp.eventContact = ecTmp;
				}
				
				//if ecType = -10101(Door 1 Sensor Battery Low) or -10102(Door 1 Sensor not responding)
				//Add ecType -10103(Door 2 Sensor Battery Low) or -10104(Door 2 Sensor not responding) Automatic
				if(ecData.eventType==-10101 || ecData.eventType==-10102){
					contactTmp = new Contact();
					contactTmp.id = CredentialGenerator.genContactId(ecTmp.id, pack.userId);
					contactTmp.userId = pack.userId;
					contactTmp.eventContact = ecTmp;
				}
			}
			
			if(ecData.eventType==10100 || ecData.eventType==10101){
				contactTmp.notifyEmail = ecData.checkEmail;
				contactTmp.notifyPushNotification = ecData.checkNotification;
				contactTmp.notifySMS = ecData.checkSMS;

				contactTmp.save();
			}
			
			if(ecData.eventType==-10101 || ecData.eventType==-10102){
				contactTmp.notifyEmail = ecData.checkEmail;
				contactTmp.notifyPushNotification = ecData.checkNotification;
				contactTmp.notifySMS = ecData.checkSMS;

				contactTmp.save();
			}
			//end
			//*/
		}

		UserRespPack respPack = new UserRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}

	@SessionInject(UserEventContactReqPack.class)
	public Result getUserContactEvent(String userId){
		UserEventContactReqPack pack = createInputPack();

		ArrayList<EventContactData> userECDataList = new ArrayList<EventContactData>();

		List<EventContact> ecList = EventContact.listByMac(pack.xGenieMAC);

		for(EventContact ec:ecList){
			
			//ignore 10102 and 10103 and -10103 and -10104 event type
			//by Jack Chuang
			if(ec.eventType!=10102 && ec.eventType!=10103 && ec.eventType!=-10103 && ec.eventType!=-10104){
			
				Contact c = Ebean.find(Contact.class, CredentialGenerator.genContactId(ec.id, userId));

				if(c!= null){
					EventContactData ecData = new EventContactData();
					ecData.eventType = ec.eventType;
					ecData.checkEmail = c.notifyEmail;
					ecData.checkNotification = c.notifyPushNotification;
					ecData.checkSMS = c.notifySMS;

					userECDataList.add(ecData);
				}
			
			}
			//end
			
			
			/*
			//ignore 10102 and 10103 event type
			//by Jack Chuang
			if(ec.eventType!=10102 && ec.eventType!=10103){
			
				Contact c = Ebean.find(Contact.class, CredentialGenerator.genContactId(ec.id, userId));

				if(c!= null){
					EventContactData ecData = new EventContactData();
					ecData.eventType = ec.eventType;
					ecData.checkEmail = c.notifyEmail;
					ecData.checkNotification = c.notifyPushNotification;
					ecData.checkSMS = c.notifySMS;

					userECDataList.add(ecData);
				}
			
			}
			//end
			
			//ignore -10103 and -10104 event type
			//by Jack Chuang
			if(ec.eventType!=-10103 && ec.eventType!=-10104){
			
				Contact c = Ebean.find(Contact.class, CredentialGenerator.genContactId(ec.id, userId));

				if(c!= null){
					EventContactData ecData = new EventContactData();
					ecData.eventType = ec.eventType;
					ecData.checkEmail = c.notifyEmail;
					ecData.checkNotification = c.notifyPushNotification;
					ecData.checkSMS = c.notifySMS;

					userECDataList.add(ecData);
				}
			
			}
			//end
			*/
		}

		UserEventContactRespPack respPack = new UserEventContactRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.ecDataList = userECDataList;

		return createJsonResp(respPack);
	}
}
