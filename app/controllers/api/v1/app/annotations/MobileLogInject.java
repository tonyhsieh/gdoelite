package controllers.api.v1.app.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.mvc.With;
import controllers.api.v1.app.MobileLogAction;
import controllers.api.v1.app.form.BaseReqPack;

/**
 * @author Jack Chuang
 *	2013 05 24
 */
@With(MobileLogAction.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MobileLogInject {

	Class<? extends BaseReqPack> value();

}