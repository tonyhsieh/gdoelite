package controllers.api.v1.app;

import java.io.IOException;
import java.util.Calendar;

import json.models.api.v1.app.BaseRespPack;
import json.models.api.v1.app.JsonStatusCode;
import json.models.api.v1.app.RespPack;
import models.User;

import com.fasterxml.jackson.databind.ObjectMapper;

import play.data.Form;
import play.mvc.Http;
import play.mvc.Result;
import controllers.BaseController;
import controllers.api.v1.app.form.BaseReqPack;
import frameworks.Constants;
import frameworks.models.StatusCode;

/**
 * @author carloxwang
 *
 */
public abstract class BaseAPIController extends BaseController {

	@SuppressWarnings("unchecked")
	protected static <T> T createInputPack(){
		return (T) Http.Context.current().args.get(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY);
	}

	protected static BaseReqPack readReqParams(Form<?> formData){
		try{
			return (BaseReqPack) formData.bindFromRequest().get();
		} catch (IllegalStateException e){
			return null;
		}

	}
	protected static Result createJsonResp(BaseRespPack respPack){

		respPack.timestamp = Calendar.getInstance();

		ObjectMapper om = new ObjectMapper();
		String responseString = "";

		try {
			responseString = om.writeValueAsString(respPack);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ok(responseString).as("application/json");
	}

	protected static Result createExceptionJsonResp(Exception e){

		RespPack respPack = new RespPack();

		respPack.status = new JsonStatusCode();

		respPack.status.code = StatusCode.UNKNOWN_ERROR.value;
		respPack.status.message = e.getMessage();

		return createJsonResp(respPack);

	}

	protected static Result createIndicatedJsonStatusResp(StatusCode status, String message){

		RespPack respPack = new RespPack();

		respPack.status = new JsonStatusCode();
		respPack.status.code = status.value;
		respPack.status.message = message;

		return createJsonResp(respPack);

	}

	protected static User getUser(){
		return (User) Http.Context.current().args.get(Constants.API_V1_APP_SESSION_HTTP_ARGS_KEY);
	}
	
	protected static Result NOP(){
		return ok("");
	}
}
