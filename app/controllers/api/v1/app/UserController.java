package controllers.api.v1.app;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import json.models.api.v1.app.ListActiveSessionRespPack;
import json.models.api.v1.app.UserRespPack;
import json.models.api.v1.app.vo.UserVO;
import models.DevicePositionLog;
import models.User;
import models.UserDevLog;
import models.UserIdentifier;

import org.apache.commons.lang3.StringUtils;


import play.mvc.Result;
import services.UserService;
import utils.IdentifierDataUtils;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.CheckkAccountAvailableReqPack;
import controllers.api.v1.app.form.GetUserInfoReqPack;
import controllers.api.v1.app.form.ListActiveSessionReqPack;
import controllers.api.v1.app.form.RegNewUserReqPack;
import controllers.api.v1.app.form.SetUserInfoReqPack;
import frameworks.Constants;
import frameworks.exceptions.FormDataValidationException;
import frameworks.models.StatusCode;
import frameworks.models.UserIdentifierType;

/**
 * @author carloxwang
 */
@PerformanceTracker
public class UserController extends BaseAPIController
{
    @Transactional
    @ParameterInject(RegNewUserReqPack.class)
    public Result regist()
    {
        RegNewUserReqPack pack = createInputPack();

        UserIdentifierType identType;
        try
        {
            identType = UserIdentifierType.valueOf(pack.loginType.toUpperCase());
        }
        catch (IllegalArgumentException e)
        {
            return badRequest();
        }

        String identData = IdentifierDataUtils.preProcessIdentifierData(pack.loginId);

        //檢查Email格式是否正確
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if (!identData.matches(EMAIL_REGEX))
        {
            return createIndicatedJsonStatusResp(StatusCode.EMAIL_FORMAT_ERROR, "The email format error.");
        }

        /// 檢查使用者重複 ////
        boolean result = UserService.checkUserExists(identData, identType);

        // User has existed. Return corresponding messages to client
        if (result)
        {
            return createIndicatedJsonStatusResp(StatusCode.USER_DUPLICATED, "The loginId has existed.");
        }

        User user = UserService.createNewUser(identType, identData, pack.loginPwd, pack.address, pack.email, pack.nickName);

        UserRespPack respPack = new UserRespPack();
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }


    @Transactional(readOnly = false)
    @SessionInject(SetUserInfoReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result setMyInfo() throws FormDataValidationException
    {
        SetUserInfoReqPack pack = createInputPack();

        User user = getUser();

        if (StringUtils.isNotBlank(pack.nickname))
        {
            user.nickname = pack.nickname;
            user.save();
        }

        //有無要求更換password
        if (StringUtils.isNotBlank(pack.loginPwd))
        {
            //取得 UserIdentifier
            List<UserIdentifier> listUserIdentifier = UserIdentifier.getListUserIdentifierByUserId(String.valueOf(user.id));

            for (int i = 0; i < listUserIdentifier.size(); i++)
            {
                UserIdentifier ui = listUserIdentifier.get(i);
                if (ui.identType == UserIdentifierType.EMAIL)
                {
                    //密碼長度要求
                    if (pack.loginPwd.length() < Constants.PASSWORD_LENGTH)
                    {
                        return createIndicatedJsonStatusResp(StatusCode.PASSWORD_LENGTH_REQUARE, "invalid password length");
                    }

                    ui.setPassword(UserIdentifier.pwdHash(pack.loginPwd));
                    ui.save();
                }
            }
        }

        UserRespPack respPack = new UserRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.myInfo = new UserVO(user);
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }


    @Transactional
    @SessionInject(GetUserInfoReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result getMyInfo() throws FormDataValidationException
    {
        GetUserInfoReqPack pack = createInputPack();

        User user = getUser();

        UserRespPack respPack = new UserRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.myInfo = new UserVO(user);
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(ListActiveSessionReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result listActiveSession() throws FormDataValidationException
    {
        ListActiveSessionReqPack pack = createInputPack();

        //先找出此 session 的 user id, 再找出所有此 user id 的 session
        User user = getUser();

        List<String> mSessions = UserSessionUtils.listSession(user.id.toString());

        ListActiveSessionRespPack respPack = new ListActiveSessionRespPack();

        UserDevLog userDevLog = null;
        DevicePositionLog devicePositionLog = null;

        for (String session : mSessions)
        {
            userDevLog = UserDevLog.getUserDevLogBySessionId(session);

            String city = "Unknown";
            //先找出時間點最近的的device_position_log
            devicePositionLog = DevicePositionLog.getDevicePositionLogByUserDevLogIdAndCurrectTime(userDevLog.id);

            Calendar createDate = devicePositionLog.createAt;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            respPack.getListActiveSession(userDevLog, city);
        }

        respPack.timestamp = Calendar.getInstance();
        respPack.sessionId = pack.sessionId;
        return createJsonResp(respPack);
    }

    @Transactional
    @ParameterInject(CheckkAccountAvailableReqPack.class)
    public Result chkAccountAvailable() throws FormDataValidationException
    {
        CheckkAccountAvailableReqPack pack = createInputPack();

        boolean result = UserService.checkUserExists(pack.loginId, UserIdentifierType.EMAIL);

        // User existed. Return corresponding messages to client
        if (result)
        {
            return createIndicatedJsonStatusResp(StatusCode.USER_DUPLICATED, "The loginId has existed.");
        }

        UserRespPack respPack = new UserRespPack();
        respPack.timestamp = Calendar.getInstance();
        return createJsonResp(respPack);
    }
}
