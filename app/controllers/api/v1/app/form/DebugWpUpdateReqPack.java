package controllers.api.v1.app.form;

public class DebugWpUpdateReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 656595556036727152L;
	
	public String no;
	
	public String price;
	
	public String tax;
	
	public String total;
	
	public String pay;
	
	public String table;
}
