package controllers.api.v1.app.form;

import java.util.List;

public class ListSwitchCfgReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 468269293013614406L;

	public List<SwitchCfg> switchData;
	
	public static class SwitchCfg{
		
		public String cfgId;
		
		public boolean avilable;
	}
}
