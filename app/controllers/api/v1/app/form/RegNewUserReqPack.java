/**
 *
 */
package controllers.api.v1.app.form;

import play.data.validation.Constraints;


/**
 * @author carloxwang
 *
 */
public class RegNewUserReqPack extends BaseReqPack {

	/**
	 *
	 */
	private static final long serialVersionUID = 9182154162306537796L;

	@Constraints.Required
	public String loginId;

	@Constraints.Required
	public String loginPwd;

	public String nickName;

	@Constraints.Required
	public String loginType;

	public String address;

	public String phoneNo;

	@Constraints.Required
	public String email;

	public String phoneInfo;

	/*
	@Override
	public String toString() {
		return "RegNewUserReqPack ["
				+ (loginId != null ? "loginId=" + loginId + ", " : "")
				+ (loginPwd != null ? "loginPwd=" + loginPwd + ", " : "")
				+ (nickName != null ? "nickName=" + nickName + ", " : "")
				+ (loginType != null ? "loginType=" + loginType + ", " : "")
				+ (address != null ? "address=" + address + ", " : "")
				+ (phoneNo != null ? "phoneNo=" + phoneNo + ", " : "")
				+ (email != null ? "email=" + email + ", " : "")
				+ (phoneInfo != null ? "phoneInfo=" + phoneInfo + ", " : "")
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (imei != null ? "imei=" + imei + ", " : "")
				+ (appVer != null ? "appVer=" + appVer + ", " : "")
				+ (deviceName != null ? "deviceName=" + deviceName + ", " : "")
				+ (deviceOsName != null ? "deviceOsName=" + deviceOsName + ", "
						: "")
				+ (deviceOsVersion != null ? "deviceOsVersion="
						+ deviceOsVersion + ", " : "")
				+ (deviceManufacturer != null ? "deviceManufacturer="
						+ deviceManufacturer + ", " : "")
				+ (deviceModule != null ? "deviceModule=" + deviceModule + ", "
						: "") + (mcc != null ? "mcc=" + mcc + ", " : "")
				+ (mnc != null ? "mnc=" + mnc + ", " : "")
				+ (lac != null ? "lac=" + lac + ", " : "")
				+ (cellId != null ? "cellId=" + cellId + ", " : "")
				+ (lat != null ? "lat=" + lat + ", " : "")
				+ (lng != null ? "lng=" + lng : "") + "]";
	}
	*/
}
