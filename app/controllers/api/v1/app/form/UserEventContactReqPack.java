/**
 * 
 */
package controllers.api.v1.app.form;
/**
 * @author johnwu
 * @version 創建時間：2013/8/19 下午5:17:05
 */

public class UserEventContactReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6429305661390360711L;

	//Because add Asante Genie Event Contact, the parameter name still keep.
	//But include AsanteGenieMAC.
	
	public String xGenieMAC;		//BTW, it might AG MAC
}
