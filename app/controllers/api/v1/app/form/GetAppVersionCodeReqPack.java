package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class GetAppVersionCodeReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7230402921232467495L;

	@Constraints.Required
	public String deviceOsName;
}
