package controllers.api.v1.app.form;

import play.data.validation.Constraints;

/**
 * 
 * @author john
 *
 */

public class LoginReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -406698072753173393L;

	@Constraints.Required
	public String loginId;
	
	@Constraints.Required
	public String loginType;
	
	public String loginPwd;
	
	@Constraints.Required
	public String pnId;
	
	@Constraints.Required
	public String deviceOsName;
	
	public String appCode;

	/*
	@Override
	public String toString() {
		return "LoginReqPack ["
				+ (loginId != null ? "loginId=" + loginId + ", " : "")
				+ (loginPwd != null ? "loginPwd=" + loginPwd + ", " : "")
				+ (pnId != null ? "pnId=" + pnId + ", " : "")
				+ (loginType != null ? "loginType=" + loginType + ", " : "");
	}
	*/
}
