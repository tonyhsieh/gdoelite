package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class GetConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3570134669153503420L;
	
	@Constraints.Required
	public String configId;
	
	@Constraints.Required
	public String cfgClass;
}
