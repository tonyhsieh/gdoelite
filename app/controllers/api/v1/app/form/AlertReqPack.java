package controllers.api.v1.app.form;

import java.util.List;

public class AlertReqPack extends BaseReqPack
{
    private static final long serialVersionUID = 3818664295002756461L;

    public String homeExtenderMac;
    public String userId;
    public String params;

    @Override
    public String toString()
    {
        return "AlertReqPack{" +
                "heMAC='" + homeExtenderMac + '\'' +
                ", userId='" + userId + '\'' +
                ", params='" + params + '\'' +
                '}';
    }
}
