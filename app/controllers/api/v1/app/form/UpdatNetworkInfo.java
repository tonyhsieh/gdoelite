package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class UpdatNetworkInfo extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6520065494816671678L;

	@Constraints.Required
	public String sessionId;
	
	public String sgMac;

	public boolean p2pSuccess;
}
