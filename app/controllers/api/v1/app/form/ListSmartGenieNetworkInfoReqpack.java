package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class ListSmartGenieNetworkInfoReqpack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8974288759417911867L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String innIp;
	
	@Constraints.Required
	public String netMask;
	
	@Constraints.Required
	public String wifiMacAddress;
}
