package controllers.api.v1.app.form;

public class DeviceLogReqPack extends BaseReqPack
{
    private static final long serialVersionUID = 7636113210500662555L;

    public String xgMAC;
    public int begin;
    public int end;
    public String order;

    @Override
    public String toString()
    {
        return "DeviceLogReqPack{" +
                "macAddr='" + xgMAC + '\'' +
                ", begin=" + begin +
                ", end=" + end +
                ", order='" + order + '\'' +
                '}';
    }
}
