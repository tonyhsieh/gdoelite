package controllers.api.v1.app.form;

import play.data.validation.Constraints;

/**
 * 
 * @author johnwu
 *
 */

public class AgreeAddUserReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2446560243902863302L;

	@Constraints.Required
	public Boolean answer;
		
	public String smartGenieId;
}
