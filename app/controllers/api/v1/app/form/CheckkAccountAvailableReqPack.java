package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class CheckkAccountAvailableReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 278012552342027794L;

	@Constraints.Required
	public String loginId;
}
