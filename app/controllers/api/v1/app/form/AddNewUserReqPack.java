package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class AddNewUserReqPack extends BaseReqPack
{
    private static final long serialVersionUID = -3109801751451436128L;

    @Constraints.Required
    public String sessionId;

    @Constraints.Required
    public String loginId;

    public String nickname;

    public String countryCode;

    public String cellPhone;

    public String gatewayId;
}
