package controllers.api.v1.app.cache;

import java.io.Serializable;

public class UserSessionData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1549411463797233728L;

	public String sessionId;

	public String userId;

	public UserSessionData(String sessionId, String userId){
		this.sessionId = sessionId;
		this.userId = userId;
	}
	
	@Override
	public String toString() {
		return "Session ["
				+ (sessionId != null ? "sessionId="	+ sessionId + ", " : "")
				+ (userId != null ? "userId=" + userId + ", " : "")
				+ "]";
	}
}