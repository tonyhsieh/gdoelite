/**
 *
 */
package controllers;

import frameworks.models.CacheKey;
import models.Event;
import utils.CacheManager;
import play.mvc.Result;

/**
 * @author carloxwang
 *
 */
public class AliveController extends BaseController {

	public Result alive(){
		//Testing Cache:
	    
	    try{

			CacheManager.set(CacheKey.SG_SESSION ,"alive_test", 1);
    		int test = (Integer) CacheManager.get(CacheKey.SG_SESSION,"alive_test");
    
    		if(test != 1){
    			return internalServerError();
    		}
    
    		// Testing ElasticSearch & DB
    		//int cnt = Ebean.find(User.class).findRowCount();
    		//Logger.info("Health Check" + cnt);
    		
    		
    		// Testing DB
    		Event event = Event.findByType(100);
		
	    }catch(Exception err){
	        return internalServerError(err.getMessage() + "");
	    }
		
		//Logger.info("Health Check");
		
//

//		IndexQuery<UserIndex> query = UserIndex.find.query();
//		query.setBuilder(QueryBuilders.queryString("just a test"));
//		query.from(0);
//		query.size(1);

//		UserIndex.find.search(query);

		// Testing Ebean
//		Ebean.createSqlQuery("SELECT 1 FROM user LIMIT 1").findUnique();

		//response().setHeader(CONNECTION, "close");
		
		return ok("ok");
	}
}
